<header>
    <nav>
        <ul id="nav_bar">
            <li id="logo"><a href="index.php">Ragtime</a></li>
            
            <li id="search_bar">
                <form action="scripts/set_search_string.php" method="get" id="search_form">
                    <input id="search_box" form="search_form" type="search" minlength='1' maxlength='64' name="search_string"/>
                    <input id="search_button" type="submit" name="submit_search" value="Search!"/>
                </form>
            </li>

            <li id="nav_spacer"></li>
            
                <?php
                if ($_SESSION['isLoggedIn']) {
                    echo '
            <li id="list_new_product"><a href="new_product.php">List a product</a></li>
            <li id="my_account"><a href="scripts/goto_personal_profile.php">My account<sup>' . $amt_notifications . '</sup></a></li>
            <li id="log_off"><a href="scripts/log_off.php">Log off</a></li>';
                } else {
                    echo "
            <li id='goto_register'><a href='registration.php'>Register</a></li>
            <li id='goto_log_in'><a href='log_in.php'>Log in</a></li>";
                }
                ?>

        </ul>
    </nav>
</header>