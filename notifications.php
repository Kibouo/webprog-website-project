<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
        <title>Ragtime.be | Notifications</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/notifications.css">
    </head>
    
    <body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php require_once 'scripts/get_notifications.php' ?>

    <div id="spacer"></div>

    <div id="page_content">
        <?php
            if ($_SESSION['isLoggedIn'])
            {
                echo '
        <div id="notifications">';

                if (sizeof($unread_notifications) + sizeof($read_notifications) == 0)
                {
                    echo '
            <h1>You don\'t seem to have any notifications.</h1>';
                } else
                {
                    echo '
            <h1>Your notifications:</h1>';
                }
            
                for ($i = 0; $i < sizeof($unread_notifications); $i++)
                {
                    echo '
            <div class="notification_new">';

                    // Test the type of the notification and output text accordingly
                    if ($unread_notifications[$i]['type'] == 'new_bidding')
                    {
                        echo 'Someone placed a new bid on one of your items. <a href="scripts/set_requested_product.php?product_id=' . $unread_notifications[$i]['about'] . '" class="notif_link">Click here</a> to view it.';
                    }
                    elseif ($unread_notifications[$i]['type'] == 'overbid')
                    {
                        echo 'Someone overbid you on an item. <a href="scripts/set_requested_product.php?product_id=' . $unread_notifications[$i]['about'] . '" class="notif_link">Click here</a> to view it.';
                    }
                    elseif ($unread_notifications[$i]['type'] == 'won_bidding')
                    {
                        echo 'You won the bidding on an item. <a href="scripts/set_requested_product.php?product_id=' . $unread_notifications[$i]['about'] . '" class="notif_link">Click here</a> to view it or <a href="scripts/goto_non-personal_profile.php?user_profile_id=' . $unread_notifications[$i]['sender_id'] . '" class="notif_link">Rate the seller</a>.';
                    }
                    elseif ($unread_notifications[$i]['type'] == 'item_deleted')
                    {
                        echo 'The item' . $unread_notifications[$i]['extra'] . 'which you were selling has been deleted.';
                    }
                    elseif ($unread_notifications[$i]['type'] == 'may_rate')
                    {
                        echo 'You ended the auctioning of an item. You can now <a href="scripts/goto_non-personal_profile.php?user_profile_id=' . $unread_notifications[$i]['sender_id'] . '" class="notif_link">Rate the buyer</a>.';
                    }

                    echo '
            </div>';
                }


                for ($i = 0; $i < sizeof($read_notifications); $i++)
                {
                    echo '
            <div class="notification_old">';

                    // Test the type of the notification and output text accordingly
                    if ($read_notifications[$i]['type'] == 'new_bidding')
                    {
                        echo 'Someone placed a new bid on one of your items. <a href="scripts/set_requested_product.php?product_id=' . $read_notifications[$i]['about'] . '" class="notif_link">Click here</a> to view it.';
                    }
                    elseif ($read_notifications[$i]['type'] == 'overbid')
                    {
                        echo 'Someone overbid you on an item. <a href="scripts/set_requested_product.php?product_id=' . $read_notifications[$i]['about'] . '" class="notif_link">Click here</a> to view it.';
                    }
                    elseif ($read_notifications[$i]['type'] == 'won_bidding')
                    {
                        echo 'You won the bidding on an item. <a href="scripts/set_requested_product.php?product_id=' . $read_notifications[$i]['about'] . '" class="notif_link">Click here</a> to view it or <a href="scripts/goto_non-personal_profile.php?user_profile_id=' . $read_notifications[$i]['sender_id'] . '" class="notif_link">Rate the seller</a>.';
                    }
                    elseif ($read_notifications[$i]['type'] == 'item_deleted')
                    {
                        echo 'The item ' . $read_notifications[$i]['extra'] . ' which you were selling has been deleted.';
                    }
                    elseif ($read_notifications[$i]['type'] == 'may_rate')
                    {
                        echo 'You ended the auctioning of an item. You can now <a href="scripts/goto_non-personal_profile.php?user_profile_id=' . $read_notifications[$i]['sender_id'] . '" class="notif_link">Rate the buyer</a>.';
                    }

                    echo '
            </div>';
                }

                echo '
        </div>';
            } else
            {
                echo '
        <div id="not_logged_in">
            <h1>Can\'t wait huh?</h1>
            <p>Log in to see your notifications.</p>
        </div>';
            }

        ?>
    </div>
    
    <?php require_once 'header.php' ?>
    </body>
</html>
