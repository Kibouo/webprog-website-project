<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title Ragtime.be | User account></title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/user.css">
	</head>
	
	<body>
        <?php require_once 'scripts/page_default_data.php' ?>
        <?php require_once 'scripts/get_user_products.php' ?>
        <?php
            // Init var to deal with 1st time visit
        if (!isset($_SESSION['requestedUser'])) {
            $_SESSION['requestedUser'] = 0;
        }
        if (!isset($_SESSION['userID'])) {
            $_SESSION['userID'] = -1;
        }
        if (!isset($_SESSION['deletedUser'])) {
            $_SESSION['deletedUser'] = true;
        }
        if (!isset($_SESSION['ratingAccepted'])) {
            $_SESSION['ratingAccepted'] = NULL;
        }
        ?>
        <?php require_once 'scripts/get_user_data.php' ?>
        <?php require_once 'scripts/test_allowed_rate.php' ?>

        <div id="spacer"></div>

        <div id="page_content">
        
        <?php

        // If the user accesses user.php correctly
        if ($reqUserFound) {
            if ($allowed_to_rate)
            {
            echo '
                <div id="rate_user">
                    <h1>Rate this user.</h1>
                    <p>How was your experience dealing with this user?</br> Let us know!</p>
                    <form action="scripts/give_rating.php" method="post">
                        <select name="rating_given" id="star_selector">
                            <option value="1">1 star</option>
                            <option value="2">2 stars</option>
                            <option value="3">3 stars</option>
                            <option value="4">4 stars</option>
                            <option value="5">5 stars</option>
                        </select>
                        <button class="button" type="submit">Rate!</button>
                    </form>
                </div>';
            }

            echo '
            <div id="user_data">
                <div id="graphical_data">
                    <img alt="User profile picture" id="profile_picture" src=' . "$avatar" . '>
                    <div id="score">';

            // Display starts based on average rating
            for ($i = 0; $i < $reqUserRating; $i++) {
                echo '  <img alt="Full rating-star" class="full_star" src="img/star_full.png">';
            }
            for ($i = 0; $i < (5 - $reqUserRating); $i++) {
                echo '  <img alt="Empty rating-star" class="empty_star" src="img/star_empty.png">';
            }

            echo '  </div>';
            
            // If the user is watching its own or admin is watching a profile, every personal info is visible
            if ($_SESSION['requestedUser'] == $_SESSION['userID'] || $_SESSION['userRole'] == "admin") {
                echo '<a class="button" href="edit_profile.php">Edit</a>';
                
                // If the user is not an admin and is watching his own, he can see his notifications
                if ($_SESSION['userRole'] != "admin" && $_SESSION['userID'] == $_SESSION['requestedUser'])
                {
                    echo '
                      <a class="button" href="notifications.php">Notifications</a>';
                // If the admin watches his own profile, he can see delete and notifs
                } elseif($_SESSION['userRole'] == "admin" && $_SESSION['userID'] == $_SESSION['requestedUser'])
                {
                    echo '
                      <a class="button" href="notifications.php">Notifications</a>
                      <a class="button" href="scripts/delete_user.php">Delete user</a>';
                // If the admin watches someone else's page, he can delete but not see notifs
                } elseif ($_SESSION['userRole'] == "admin" && $_SESSION['userID'] != $_SESSION['requestedUser'])
                {
                    echo '
                      <a class="button" href="scripts/delete_user.php">Delete user</a>';
                } // The other possibility is a normal user watching someonoe else's profile, which results in only the Edit button showing.

                echo '
                </div>
                <div class="written_data">';

                if (!$_SESSION['deletedUser']) {
                    echo '<div class="error">Failed to delete this user.</div>';
                    $_SESSION['deletedUser'] = true;
                }

                echo '
                    <div class="username"><b>Username:</b> ' . "$username" . '</div>';
                
                if (!empty($email)) {
                    echo '
                    <div class="email"><b>E-mail:</b> ' . "$email" . '</div>';
                }
                if (!empty($telnr)) {
                    echo '
                    <div class="tel_nr"><b>Telephone no.:</b> ' . "$telnr" . '</div>';
                }
                if (!empty($street) && !empty($housenr) && !empty($postcode) && !empty($country)) {
                    echo '
                    <div class="location"><b>Location:</b> ' . "$street" . ' ' . "$housenr" . '</br>' . "$postcode" . ' ' . "$country" . '</div>';
                }
                if (!empty($description)) {
                    echo '
                    <p class="description"><b>Personal description:</b> ' . "$description" . '</p>';
                }
                   echo '
                </div>
            </div>';
            } // If user is watching someone else's profile, only info is shown which the watchee has approved of showing
            else {
                echo '
                </div>
                <div class="written_data">';

                if (is_null($_SESSION['ratingAccepted']))
                {
                    // If null (nothing special happened), don't output anything.
                } elseif (!$_SESSION['ratingAccepted']) {
                    echo '<div class="error">Failed to process your rating.</div>';
                    $_SESSION['ratingAccepted'] = NULL;
                } elseif ($_SESSION['ratingAccepted']) {
                    echo '<div class="success">Your rating has been received.</div>';
                    $_SESSION['ratingAccepted'] = NULL;
                }

                echo '
                    <div class="username"><b>Username:</b> ' . "$username" . '</div>';

                if ($_SESSION['showEmail'] == 1 && !empty($email)) {
                    echo '
                    <div class="email"><b>E-mail:</b> ' . "$email" . '</div>';
                }
                if ($_SESSION['showTelephone'] == 1 && !empty($telnr)) {
                    echo '
                    <div class="tel_nr"><b>Telephone no.:</b> ' . "$telnr" . '</div>';
                }
                if ($_SESSION['showLocation'] == 1 && !empty($street) && !empty($housenr) && !empty($postcode) && !empty($country)) {
                    echo '
                    <div class="location"><b>Location:</b> ' . "$street" . ' ' . "$housenr" . '</br>' . "$postcode" . ' ' . "$country" . '</div>';
                }
                if (!empty($description)) {
                    echo '
                    <p class="description"><b>Personal description:</b> ' . "$description" . '</p>';
                }
                echo '
                </div>
            </div>';
            }
            
            echo '
            <div id="products">
                <h1 id="products_title">All products sold by ' . "$username" . ':</h1>';

            if (sizeof($prod_data) == 0) {
                echo '
                <p id="no_item">This user doesn\'t sell any items.</p>';
            } else {
                // Reverse loop because the last edited item (last bid on = most popular) will at the end of the list, thus now as 1st item. Kinda QoL since we don't need to check on date here.
                for ($i = sizeof($prod_data) - 1; $i >= 0; $i--) {
                    $tmp_id = $prod_data[$i]["id"];
                    $tmp_picture = $prod_data[$i]["picture"];
                    $tmp_price = number_format($prod_data[$i]["price"], 2, ",", " ");
                    $tmp_category = $prod_data[$i]["category"];

                    if (strlen($prod_data[$i]["title"]) > 16) {
                        $tmp_title = substr($prod_data[$i]["title"], 0, 16) . "...";
                    } else {
                        $tmp_title = $prod_data[$i]["title"];
                    }
                
                    if (strlen($prod_data[$i]["descr"]) > 16) {
                        $tmp_description = substr($prod_data[$i]["descr"], 0, 16) . "...";
                    } else {
                        $tmp_description = $prod_data[$i]["descr"];
                    }

                    echo '
                        <a href="scripts/set_requested_product.php?product_id=' . $tmp_id . '">
                            <div class="product">
                                <img alt="Product picture" class="prod_picture" src=' . $tmp_picture . '>
                                <div class="prod_data">    
                                    <h2 class="title">' . $tmp_title . '</h2>
                                    <div class="price">Price: €' . $tmp_price . '</div>
                                    <div class="tags">' . $tmp_category . '</div>
                                </div>
                                <p class="description">Description: ' . $tmp_description . '</p>
                            </div>
                        </a>';
                }
            }
        } else {
            echo '
                <div id="reqUserError">
                    <h1>Woops...</h1>
                    <p>This user doesn\'t seem to exist.</p>
                </div>';
        }

        ?>

        </div>
        </div>

    <?php require_once 'header.php' ?>
    </body>
</html>
