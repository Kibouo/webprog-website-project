<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
        <title>Ragtime.be | Product info</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/product.css">
    </head>
    
    <body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php
    if (isset($_SESSION['requestedProduct'])) {
        require_once 'scripts/get_prod_data.php';
    }
    ?>
    <?php
    // Init var to deal with 1st time visit
    if (!isset($_SESSION['bidAccepted'])) {
        $_SESSION['bidAccepted'] = true;
    }
    if (!isset($_SESSION['closeBiddingSuccess'])) {
        $_SESSION['closeBiddingSuccess'] = -1;
    }
    if (!isset($_SESSION['deletedProduct'])) {
        $_SESSION['deletedProduct'] = true;
    }
    ?>

    <div id="spacer"></div>

    <div id="page_content">

    <?php
    if (isset($_SESSION['requestedProduct']) && isset($prod_data['id'])) {
        $tmp_price = number_format($prod_data['price'], 2, ",", " ");
        $tmp_title = wordwrap($prod_data['title'], 24, "\n", true);
        $tmp_description = wordwrap($prod_data['descr'], 80, "\n", true);
        $tmp_category = urlencode($prod_data['category']);

        echo '
        <div id="product">';

        if (!$_SESSION['bidAccepted']) {
            echo '
            <div class="error">Error while processing your bid. Verify you bid higher than the current price and try again.
            </div>';
            $_SESSION['bidAccepted'] = true;
        }

        if ($_SESSION['closeBiddingSuccess'] != -1) {
            if (!$_SESSION['closeBiddingSuccess']) {
                echo '
                <div class="error">We were unable to close this bidding.
                </div>';
                $_SESSION['closeBiddingSuccess'] = -1;
            } elseif ($_SESSION['closeBiddingSuccess']) {
                echo '
                <div class="success">You successfully accepted the offer.
                </div>';
                $_SESSION['closeBiddingSuccess'] = -1;
            }
        }

        if (!$_SESSION['deletedProduct']) {
            echo '
                <div class="error">Failed to delete this article.</div>';
                $_SESSION['bidAccepted'] = true;
        }

        if ($prod_data['highest_bidder'] == $_SESSION['userID'] && $_SESSION['isLoggedIn']) {
            echo '
            <div class="success">
            You\'re currently the highest bidder on this article.
            </div>';
        }

        echo '
            <div>';

        echo '
                <div id="prod_picture">
                    <img alt="Product picture" id="picture" src=' . $prod_data['picture'] . '>
                </div>
                    
                <div id="prod_data">
                    <h1 id="title">' . $tmp_title . '</h1>
                    <div id="price">Current price: €' . $tmp_price . '</div>';

        if ($prod_data['bidding_closed']) {
            echo '      
                    <div id="sold">This item has been sold.</div>';

            if ($_SESSION['userRole'] == "admin") {
                echo '
                    <a id="delete_user" href="scripts/delete_product.php">Delete product</a>';
            }
        } elseif ($_SESSION['isLoggedIn']) {
            if ($_SESSION['userID'] == $prod_data['seller'] && $prod_data['highest_bidder'] != null) {
                echo '<a href="scripts/end_bidding.php" id="close_bidding_button">Accept offer</a>';
            } elseif ($_SESSION['userID'] != $prod_data['seller']) {
                echo '
                    <form id="place_bid" action="scripts/update_price_db_connect.php" method="post">
                        <input type="number" id="bid_textbox" name="bid_price" inputmode="numeric" step="0.01" min="0" placeholder="Your bid (€)"/>
                        <button id="bid_button" type="submit">Place bid</button>
                    </form>';
            }

            if ($_SESSION['userRole'] == "admin") {
                echo '
                    <a id="delete_user" href="scripts/delete_product.php">Delete product</a>';
            }
        } else {
            echo '      
                    <div id="offline">Log in to bid on this item.</div>';
        }
        
        echo '
                    <div id="tags">Category: <a id="category_link" href="scripts/set_requested_category.php?product_category=' . $tmp_category . '">' . $prod_data['category'] . '</a></div>
                    <div id="seller">Seller: <a id="seller_id_link" href="scripts/goto_non-personal_profile.php?user_profile_id=' . $prod_data['seller'] . '">' . $seller_username['username'] . '</a></div>
                    <div>Posted on: ';
        echo            gmdate("d M Y", $prod_data['date_time']);
        echo '      </div>
                </div>
            </div>
            
            <div>
                <div id="product_details">
                    <p id="description">Description: ' . $tmp_description . '</p>
                    <div id="product_media">';

        for ($i = 0; $i < sizeof($prod_media_data); $i++) {
            if (substr($prod_media_data[$i]['link'], -4) == ".mp4") {
                echo '
                         <video class="media" src=' . $prod_media_data[$i]['link'] . ' controls>
                         Your browser does not support the video tag.
                         </video>';
            } else {
                echo '
                        <img alt="Additional product pictures" class="media" src=' . $prod_media_data[$i]['link'] . '>';
            }
        }

        echo '
                    </div>
                </div>
            </div>
        </div>';
    } else {
        echo '
        <div id="error_dialog">
            <h1>Something went wrong</h1>
            <p>Try selecting an article you\'re interested in.</p>
        </div>';
    }
    ?>
    </div>

    <?php require_once 'header.php' ?>
    </body>
</html>
