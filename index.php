<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | The smell will wash out</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/index.css">
	</head>
	
	<body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php require_once 'scripts/get_categories.php' ?>
    <?php
        $products_array = new ArrayObject();
        $_SESSION['amt_index_products'] = 0;
    ?>
    <?php require 'scripts/get_basic_products.php' ?>
    
    <div id="spacer"></div>

    <div id="page_content">
    
    <?php
    if (!$_SESSION['isLoggedIn']) {
        echo '
        <div id="welcome_tab">
            <h1>Welcome</h1>
            <p>
                Join now and sell your unused items within mere minutes!
            </p>
            <a href="registration.php" id="welcome_register">Register</a>
            <a href="log_in.php" id="welcome_log_in">Log in</a>
        </div>';
    }

    echo '
        <div id="choose_category">
            <h1>Choose a category</h1>
            <p>Search for all products in a category:</p>
            <form id="category_dropdown" action="scripts/set_requested_category.php" method="post">
                <select id="dropdown" name="product_category">';

    for ($i = 0; $i < sizeof($categories); $i++) {
        $tmp = implode("", $categories[$i]);
        echo '
                    <option value=' . $tmp . '>' . $tmp . '</option>';
    }

    echo '
                </select>
                <input type="submit" id="set_req_category" name="set_req_category" value="Search!"/>
            </form>
        </div>';

    // Print products
    for ($i = 0; $i < sizeof($products_array); $i++)
    {
        $tmp_price = number_format($products_array[$i]['price'], 2, ",", " ");
        $tmp_title = wordwrap($products_array[$i]['title'], 24, "\n", true);
        $tmp_category = urlencode($products_array[$i]['category']);
        echo '
        <a class="item_link" href="scripts/set_requested_product.php?product_id=' . $products_array[$i]['id'] . '">
            <div class="filtered_product">
                <img alt="Product picture" class="prod_picture" src="' . $products_array[$i]['picture'] . '">
                <h1 class="prod_title">' . $tmp_title . '</h1>
                <div class="price">Price: €' . $tmp_price . '</div>
                <div class="tags">Category: ' . $products_array[$i]['category'] . '</div>
            </div>
        </a>';
    }
    ?>
    </div>
    
    <?php require_once 'header.php' ?>
    <script src="scripts/scroll_load.js"></script>
    </body>
</html>
