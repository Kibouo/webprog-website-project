<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
  <title Ragtime.be | User account></title>
  <link rel="stylesheet" href="css/header.css">
  <link rel="stylesheet" href="css/edit_profile.css">
</head>

<body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php require_once 'scripts/get_privacy_data.php' ?>
    <?php

    // Init flags for 1st time visiting page
    if (!isset($_SESSION['edit_profile_update_avatar'])) {
        $_SESSION['edit_profile_update_avatar'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_username'])) {
        $_SESSION['edit_profile_update_username'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_password'])) {
        $_SESSION['edit_profile_update_password'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_street'])) {
        $_SESSION['edit_profile_update_street'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_housenr'])) {
        $_SESSION['edit_profile_update_housenr'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_postal_code'])) {
        $_SESSION['edit_profile_update_postal_code'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_country'])) {
        $_SESSION['edit_profile_update_country'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_tel_nr'])) {
        $_SESSION['edit_profile_update_tel_nr'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_description'])) {
        $_SESSION['edit_profile_update_description'] = true;
    }
    if (!isset($_SESSION['edit_profile_update_privacy'])) {
        $_SESSION['edit_profile_update_privacy'] = true;
    }
    if (!isset($_SESSION['edit_profile_successful'])) {
        $_SESSION['edit_profile_successful'] = false;
    }
    ?>

    <div id="spacer"></div>

    <div id="page_content">
        <?php

        if ($_SESSION['isLoggedIn']) {
            echo '
        <div id="account">';

            if ($_SESSION['edit_profile_successful']) {
                echo '
            <div id=success>
                <p>Successfully updated your profile!</p>
            </div>';
                $_SESSION['edit_profile_successful'] = false;
            } elseif (!$_SESSION['edit_profile_update_avatar'] || !$_SESSION['edit_profile_update_username'] || !$_SESSION['edit_profile_update_password'] || !$_SESSION['edit_profile_update_street'] || !$_SESSION['edit_profile_update_housenr'] || !$_SESSION['edit_profile_update_postal_code'] || !$_SESSION['edit_profile_update_country'] || !$_SESSION['edit_profile_update_tel_nr'] || !$_SESSION['edit_profile_update_description'] || !$_SESSION['edit_profile_update_privacy']) {
                echo '
            <div id=error>';
                if (!$_SESSION['edit_profile_update_privacy']) {
                    echo '
                    <p>We failed to update your privacy choices.</p>';
                    $_SESSION['edit_profile_update_privacy'] = true;
                }
                echo '
            <p>Something went wrong. Remember:</p>
            <lu>';
        
                if (!$_SESSION['edit_profile_update_avatar']) {
                    echo '
                <li>the avatar must be a picture.</li>
                <li>the avatar must be smaller than 2MB.</li>';
                    $_SESSION['edit_profile_update_avatar'] = true;
                }
                if (!$_SESSION['edit_profile_update_username']) {
                    echo '
                <li>the username must be between 4 and 64 characters long.</li>';
                    $_SESSION['edit_profile_update_username'] = true;
                }
                if (!$_SESSION['edit_profile_update_password']) {
                    echo '
                <li>the old password must be correct.</li>
                <li>the new passwords must match.</li>
                <li>the new password must be between 8 and 72 characters.</li>';
                    $_SESSION['edit_profile_update_password'] = true;
                }
                if (!$_SESSION['edit_profile_update_street']) {
                    echo '
                <li>the street has a maximum length of 128 characters.</li>';
                    $_SESSION['edit_profile_update_street'] = true;
                }
                if (!$_SESSION['edit_profile_update_housenr']) {
                    echo '
                <li>the house Nr. has a maximum length of 8 characters.</li>';
                    $_SESSION['edit_profile_update_housenr'] = true;
                }
                if (!$_SESSION['edit_profile_update_postal_code']) {
                    echo '
                <li>the postal code has a maximum length of 16 characters.</li>';
                    $_SESSION['edit_profile_update_postal_code'] = true;
                }
                if (!$_SESSION['edit_profile_update_country']) {
                    echo '
                <li>the country has a maximum length of 64 characters.</li>';
                    $_SESSION['edit_profile_update_country'] = true;
                }
                if (!$_SESSION['edit_profile_update_tel_nr']) {
                    echo '
                <li>the telephone nr. has a maximum length of 16 characters.</li>';
                    $_SESSION['edit_profile_update_tel_nr'] = true;
                }
                if (!$_SESSION['edit_profile_update_description']) {
                    echo '
                <li>the user description has a maximum length of 512 characters.</li>';
                    $_SESSION['edit_profile_update_description'] = true;
                }
        
                echo '
            </lu>
            </br>
            </div>';
            }
    
            echo '
            <form enctype="multipart/form-data" action="scripts/edit_profile_db_connect.php" method="post">
            <div id="profile">
                <input type="hidden" name="max_file_size" value="2000000">
                <label for="user_picture">New avatar<input type="file" id="user_picture" name="user_picture" accept="image/*" autocomplete="photo"></label>
                <input type="text" id="username_new" name="user_name" autocomplete="username" minlength="4" maxlength="64" placeholder="Username">
            </div>';

            // Admins shouldn't be able to edit passwords. Especially because they don't know the old pass of the user.
            if ($_SESSION['userRole'] != "admin") {
                echo '
            <div id="password">
                <input type="password" id="old_pass" name="user_old_pass" autocomplete="current-password" minlength="8" maxlength="72" placeholder="Old password">
                <input type="password" id="new_pass" name="user_new_pass" autocomplete="new-password" minlength="8" maxlength="72" placeholder="New password">
                <input type="password" id="new_pass_repeat" name="user_new_pass_repeat" autocomplete="new-password" minlength="8" maxlength="72" placeholder="Repeat password">
            </div>';
            }

            echo '
            <div id="detailed_information">
                <input type="text" id="new_user_street" name="user_street" maxlength="128" autocomplete="street-address" placeholder="Street">
                <input type="text" id="new_user_housenr" name="user_housenr" maxlength="8" inputmode="numeric" placeholder="Nr.">
                <input type="text" id="new_user_postal_code" name="user_postal_code" maxlength="16" autocomplete="postal-code" inputmode="numeric" placeholder="Postal code">
                <input type="text" id="new_user_country" name="user_country" maxlength="64" autocomplete="country" placeholder="Country">
                <input type="tel" id="tel_nr_new" name="user_tel_nr" maxlength="16" autocomplete="tel" inputmode="tel" placeholder="Telephone number">
                <textarea id="description_new" name="user_description" maxlength="512" placeholder="Personal description"></textarea>
            </div>

                <p>Privacy settings:</p>
            <div id="privacy_settings">
                <div>';
            if ($_SESSION['showEmail'] == 1) {
                echo '
                    <input type="checkbox" id="checkbox_show_email" name="user_show_email" value="1" checked>';
            } else {
                echo '
                    <input type="checkbox" id="checkbox_show_email" name="user_show_email" value="1">';
            }
                echo '
                <label for="checkbox_show_email">Show e-mail</label>
                </div>
                
                <div>';
            if ($_SESSION['showTelephone'] == 1) {
                echo '
                <input type="checkbox" id="checkbox_show_telephone" name="user_show_telephone" value="1" checked>';
            } else {
                echo '
                <input type="checkbox" id="checkbox_show_telephone" name="user_show_telephone" value="1">';
            }
                echo '
                <label for="checkbox_show_telephone">Show telephone nr.</label>
                </div>

                <div>';
            if ($_SESSION['showLocation'] == 1) {
                echo '
                <input type="checkbox" id="checkbox_show_location" name="user_show_location" value="1" checked>';
            } else {
                echo ' <input type="checkbox" id="checkbox_show_location" name="user_show_location" value="1">';
            }
                echo '
                <label for="checkbox_show_location">Show location</label>
                </div>
            </div>

            <div id="submit_changes">
                <input type="submit" name="submit_changes" value="Submit changes">
            </div>';

            echo '
            </form>
        </div>';
        } else {
            echo '
        <div id="not_logged_in">
            <h1>Patience young padawan</h1>
            <p>You must first log in to be able to edit your profile.</p></br>
        </div>';
        }
    ?>

        </div>
    <?php require_once 'header.php' ?>
    </body>
</html>
