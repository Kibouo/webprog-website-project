<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | Registration</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/registration.css">
	</head>
	
	<body>
        <?php require_once 'scripts/page_default_data.php' ?>
        <?php
        // Init flags for when 1st time visiting page
        if (!isset($_SESSION['registrationEmailAvailable'])) {
            $_SESSION['registrationEmailAvailable'] = true;
        }
        if (!isset($_SESSION['registrationRequirementsAccepted'])) {
            $_SESSION['registrationRequirementsAccepted'] = true;
        }

        ?>   
    
        <div id="spacer"></div>

        <div id="page_content">

                <?php

                // If user not logged in
                if (!$_SESSION['isLoggedIn']) {
                    echo '
            <form id="registration" action="scripts/registration_db_connect.php" method="post">
                <div id="register">
                    <h1 class="title">Registration</h1>';

                    if (!$_SESSION['registrationEmailAvailable']) {
                        echo '
                    <p class="error">This e-mail address is already in use.</p>';
                        $_SESSION['registrationEmailAvailable'] = true;
                    } elseif (!$_SESSION['registrationRequirementsAccepted']) {
                        echo '
                    <p class="error">Some requirements were not met. Try again.</p>';
                        $_SESSION['registrationRequirementsAccepted'] = true;
                    }

                    echo '
                    <input type="email" id="email" name="user_email" autocomplete="email" autofocus inputmode="email" placeholder="example@email.com" maxlength="64" required></input>
                    <input type="password" id="new_pass" name="user_pass" autocomplete="new-password" inputmode="verbatim" minlength="8" maxlength="72" placeholder="Password" required></input>	
                    <input type="password" id="new_pass_repeat" name="user_pass_repeat" autocomplete="new-password" inputmode="verbatim" minlength="8"  maxlength="72" placeholder="Repeat password" required></input>
                    
                    <input id="register_button" type="submit" name="submit" value="Register!">
                </div>
            </form>';
                } else // If user is logged in
                {
                    echo '
            <div id=already_logged_in>
                <h1 class="title">Again?</h1>
                <p>It seems you already are a part of our community. Why not check out the rest of the website?</p></br>
            </div>';
                }

                ?>


        </div>
        <?php require_once 'header.php' ?>
    </body>
</html>
