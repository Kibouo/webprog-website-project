<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | Accessibility statement</title>
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/accessibility_statement.css">
		</head>
		
		<body>
			<?php require_once 'scripts/page_default_data.php' ?>

			<div id="spacer"></div>

			<div id="page_content">
				<div id="statement">
					<h1>Accessibility statement</h1>
					<p>This website has several accessibility statements:</p>
					<ol>
						<li>All content images used in this site include descriptive ALT attributes.</li>
						<li>There are no images used for navigation.</li>
						<li>The focus of our page design is always to have an as uncluttered page as possible.</li>
						<li>We try to use as many big buttons as possible for easier navigation.</li>
						<li>Where possible, appropriate INPUTMODE attributes are used for input fields.</li>
						<li>Our webpage works with flex boxes. This results in a very responsive design, fitting for a wide range of devices and screen resolutions/sizes.</li>
						<li>To assist screen readers, we placed the navigation bar which you see on top of the page, at the bottom of our code.</li>
						<li>On every page, we specify the correct content type.</li>
						<li>We try to use meaningful page titles on all our pages.</li>
						<li>Even without JavaScript, all our pages work to their full extent.</li>
					</ol>
				</div>
			</div>

			<?php require_once 'header.php' ?>
		</body>
</html>