<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | List a new product</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/new_product.css">
	</head>
	
	<body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php require_once 'scripts/get_categories.php' ?>
    <?php

    // Init flags for 1st time visiting page
    if (!isset($_SESSION['list_new_product_success'])) {
        $_SESSION['list_new_product_success'] = false;
    }
    if (!isset($_SESSION['product_data_format'])) {
        $_SESSION['product_data_format'] = true;
    }
    if (!isset($_SESSION['product_media_uploaded'])) {
        $_SESSION['product_media_uploaded'] = true;
    }
    ?>
   
    <div id="spacer"></div>

    <div class="page_content">
        <?php

        if ($_SESSION['isLoggedIn']) {
            echo '
        <div id="logged_in">';

            // If successfully posted a product
            if ($_SESSION['list_new_product_success']) {
                echo '
            <div id="success">
                <p>Successfully created your article!</p>
                <a href="product.php">Click here to see the article.</a>
            </div>';

                $_SESSION['list_new_product_success'] = false;
            } elseif (!$_SESSION['product_data_format'] || !$_SESSION['product_media_uploaded']) {
                echo '
            <div id="error">';

                if (!$_SESSION['product_data_format']) {
                    echo '
                <p>Please fill out all fields. Files cannot be bigger than 2MB.</p>';

                    $_SESSION['product_data_format'] = true;
                }
                if (!$_SESSION['product_media_uploaded']) {
                    echo '
                <p>We failed to process the media you provided.</p>';

                    $_SESSION['product_media_uploaded'] = true;
                }
            
                echo '
            </div>';
            }
            echo '
            <form id="new_product_form" enctype="multipart/form-data" action="scripts/new_product_db_connect.php" method="post">
                <div id="new_product">
                    <div>
                        <input type="hidden" name="max_file_size" value="2000000">
                        <label for="product_picture">Product picture<input type="file" id="product_picture" name="product_picture" accept="image/*" autocomplete="photo"/></label>
                        <input type="text" id="title" name="product_title" minlength="4" maxlength="128" placeholder="Product title"/>
                    </div>
                    
                    <div>
                        <input type="number" id="min_price" name="product_price" step="0.01" min=0 placeholder="Min. price (€)"/>
                        <select id="category_selector" name="product_category">';

            for ($i = 0; $i < sizeof($categories); $i++) {
                $tmp = implode("", $categories[$i]);
                echo '
                            <option value=' . $tmp . '>' . $tmp . '</option>';
            }

            echo '
                        </select>
                        <input type="hidden" name="max_file_size" value="2000000">
                        <label for="product_media">Additional media<input type="file" id="product_media" name="product_media[]" accept="image/*,video/*" multiple/></label>
                    </div>
                    
                    <div>
                        <textarea id="description" name="product_description" maxlength="512" placeholder="Product description"></textarea>     
                    </div>
                    
                    <div>
                        <input type="submit" id="list_prod_button" name="list_prod_button" value="List item"/>
                    </div>
                </div> 
            </form>
        </div>';
        } else {
            echo '
        <div id="not_logged_in">
            <h1>You seem eager to join us...</h1>
            <p>Quick, log in or register to list a product!</p></br>
        </div>';
        }
        ?>
    </div>
    
	<?php require_once 'header.php' ?>
    </body>
</html>
