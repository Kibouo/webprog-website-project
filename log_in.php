<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | Log in</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/log_in.css">
	</head>
	
	<body>
        <?php require_once 'scripts/page_default_data.php' ?>
        <?php

        // Init flags for when 1st time visiting page
        if (!isset($_SESSION['log_inRequirementsAccepted'])) {
            $_SESSION['log_inRequirementsAccepted'] = true;
        }
        if (!isset($_SESSION['log_inUserExists'])) {
            $_SESSION['log_inUserExists'] = true;
        }
        if (!isset($_SESSION['log_inUsername_passwordCorrect'])) {
            $_SESSION['log_inUsername_passwordCorrect'] = true;
        }

        ?>  

        <div id="spacer"></div>

        <div id="page_content">

            <?php

            // If user is not already logged in
            if (!$_SESSION['isLoggedIn']) {
                echo '
            <form id="credentials" action="scripts/log_in_db_connect.php" method="post">
            <div id="log_in">
                <h1 class="title">Log in</h1>';
                       
                if (!$_SESSION['log_inRequirementsAccepted']) {
                    echo '
                <p class="error">Something went wrong. Try again.</p>';
                    $_SESSION['log_inRequirementsAccepted'] = true;
                } elseif (!$_SESSION['log_inUserExists']) {
                    echo '
                <p class="error">The provided username or password is incorrect.</p>';
                    $_SESSION['log_inUserExists'] = true;
                } elseif (!$_SESSION['log_inUsername_passwordCorrect']) {
                    echo '
                <p class="error">The provided username or password is incorrect.</p>';
                    $_SESSION['log_inUsername_passwordCorrect'] = true;
                }

                echo '
                <input type="email" id="email" name="user_email" autocomplete="email" autofocus inputmode="email" placeholder="user@email.com" maxlength="64" required></input>
                <input type="password" id="acc_pass" name="user_pass" minlength="8" maxlength="72" autocomplete="current-password" placeholder="Password" required></input>
                        
                <input id="log_in_button" type="submit" name="submit" value="Log in">
                        
                <p>Don\'t have an account yet?</p>
                <a id="go_register" href="registration.php">Register</a>

                <p>Interested in our additional efforts?</p>
                <a id="goto_accessibility_statement" href="accessibility_statement.php">See our accessibility statement!</a>
            </div>
            </form>';
            } else // If user is already logged in
                {
                    echo '
            <div id=already_logged_in>
                <h1 class="title">Again?</h1>
                <p>You\'re already successfully logged in. Why not have a look at the rest of our site?</p></br>
            </div>';
            }

                ?>
   
        </div>
        
    <?php require_once 'header.php' ?>
    </body>
</html>
