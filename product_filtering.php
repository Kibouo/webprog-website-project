<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | Product filtering</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/product_filtering.css">
	</head>
	
	<body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php require_once 'scripts/get_categories.php' ?>
    <?php require_once 'scripts/get_filtered_prod.php' ?>
    
    <div id="spacer"></div>

    <div class="page_content">
        <div id="filtering_params">
            <form action="product_filtering.php" method="get" id="filter_optimize">
                <div id="dropdown_filters">
                    <select name="search_in">
                    <?php
                    if ($_REQUEST['search_in'] == "title_descr") {
                        echo '
                        <option value="title_descr" selected="selected">Title & description</option>';
                    } else {
                        echo '
                        <option value="title_descr">Title & description</option>';
                    }
                    if ($_REQUEST['search_in'] == "title") {
                        echo '
                        <option value="title" selected="selected">Title</option>';
                    } else {
                        echo '
                        <option value="title">Title</option>';
                    }
                    if ($_REQUEST['search_in'] == "descr") {
                        echo '
                        <option value="descr" selected="selected">Description</option>';
                    } else {
                        echo '
                        <option value="descr">Description</option>';
                    }
                    ?>
                    </select>
                            
                    <select name="order_by">
                    <?php
                    if ($_REQUEST['order_by'] == "") {
                        echo '
                        <option value="" selected="selected">Latest biddings</option>';
                    } else {
                        echo '
                        <option value="">Latest biddings</option>';
                    }
                    if ($_REQUEST['order_by'] == "date_new") {
                        echo '
                        <option value="date_new" selected="selected">Date: newest first</option>';
                    } else {
                        echo '
                        <option value="date_new">Date: newest first</option>';
                    }
                    if ($_REQUEST['order_by'] == "date_old") {
                        echo '
                        <option value="date_old" selected="selected">Date: oldest first</option>';
                    } else {
                        echo '
                        <option value="date_old">Date: oldest first</option>';
                    }
                    if ($_REQUEST['order_by'] == "price_high") {
                        echo '
                        <option value="price_high" selected="selected">Price: highest first</option>';
                    } else {
                        echo '
                        <option value="price_high">Price: highest first</option>';
                    }
                    if ($_REQUEST['order_by'] == "price_low") {
                        echo '
                        <option value="price_low" selected="selected">Price: lowest first</option>';
                    } else {
                        echo '
                        <option value="price_low">Price: lowest first</option>';
                    }
                    ?>
                    </select>
               
                    <input type="submit" name="add_filter" value="Specify filter"/>
                </div>
               
                <div id="checkbox_filters">

                <?php
                for ($i = 0; $i < sizeof($categories); $i++) {
                    $tmp = implode("", $categories[$i]);
                    if (isset($_REQUEST['cbox'.$i])) {
                        echo '
                    <input type="checkbox" checked name="cbox' . $i . '" id="cbox' . $i . '" value="' . $tmp . '">
                    <label for="cbox' . $i . '">' . $tmp . '</label>';
                    } else {
                        echo '
                    <input type="checkbox" name="cbox' . $i . '" id="cbox' . $i . '" value="' . $tmp . '">
                    <label for="cbox' . $i . '">' . $tmp . '</label>';
                    }
                }
                ?>
                </div>
            </form>
        </div>
         
        <div id="filtered_products">
        <?php

        if (sizeof($prod_data) == 0) {
            echo '<p id="no_prod">No products found.</p>';
        } else {
            for ($i = 0; $i < sizeof($prod_data); $i++) {
                $tmp_id = $prod_data[$i]["id"];
                $tmp_picture = $prod_data[$i]["picture"];
                $tmp_price = number_format($prod_data[$i]["price"], 2, ",", " ");
                $tmp_category = $prod_data[$i]["category"];

                if (strlen($prod_data[$i]["title"]) > 16) {
                    $tmp_title = substr($prod_data[$i]["title"], 0, 16) . "...";
                } else {
                    $tmp_title = $prod_data[$i]["title"];
                }
                
                if (strlen($prod_data[$i]["descr"]) > 16) {
                    $tmp_description = substr($prod_data[$i]["descr"], 0, 16) . "...";
                } else {
                    $tmp_description = $prod_data[$i]["descr"];
                }
                
                    echo '
            <a href="scripts/set_requested_product.php?product_id=' . $tmp_id . '">
                <div class="filtered_product">
                    <img alt="Product picture" class="picture" src=' . $tmp_picture . '>
                    <div class="prod_data">    
                        <h2 class="title">' . $tmp_title . '</h2>
                        <div class="price">Price: €' . $tmp_price . '</div>
                    </div>
                    <p class="category">' . $tmp_category . '</p>
                    <p class="description">Description: ' . $tmp_description . '</p>
                </div>
            </a>';
            }
        }
        ?>
        
        </div>
    </div>

    <?php require_once 'header.php' ?>
    </body>
</html>
