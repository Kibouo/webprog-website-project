<?php

session_start();

require_once 'globals.php';
require_once 'get_prod_data.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$_SESSION['deletedProduct'] = false;

// If requesting user is admin (so no illegal access to this script), delete the requested product
if ($_SESSION['userRole'] == "admin") {
    $delete_prod_query = $connection->prepare('DELETE FROM products
                                                WHERE id = :id');
    $delete_prod_query->bindParam(':id', $_SESSION['requestedProduct'], PDO::PARAM_INT);

    try {
        $delete_prod_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $_SESSION['deletedProduct'] = true;
}


// If product was successfully deleted, notify highest bidder and seller
if ($_SESSION['deletedProduct'])
{
    $date_time = time();

    // Notify seller of product being deleted
    $item_deleted_notif = 'item_deleted';

    $notify_seller = $connection->prepare('INSERT INTO notifications(receiver_id, sender_id, type, about, extra, date_time)
                                            VALUES (:receiver_id, :sender_id, :type, :about, :extra, :date_time)');
    $notify_seller->bindParam(':receiver_id', $prod_data['seller'], PDO::PARAM_INT);
    $notify_seller->bindParam(':sender_id', $_SESSION['userID'], PDO::PARAM_INT);
    $notify_seller->bindParam(':type', $item_deleted_notif, PDO::PARAM_STR, 16);
    $notify_seller->bindParam(':about', $_SESSION['requestedProduct'], PDO::PARAM_INT);
    $notify_seller->bindParam(':extra', $prod_data['title'], PDO::PARAM_STR, 128);
    $notify_seller->bindParam(':date_time', $date_time, PDO::PARAM_INT);

    try {
        $notify_seller->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    // Notify last highest bidder of item being deleted
    $notify_last_bidder = $connection->prepare('INSERT INTO notifications(receiver_id, sender_id, type, about, extra, date_time)
                                            VALUES (:receiver_id, :sender_id, :type, :about, :extra, :date_time)');
    $notify_last_bidder->bindParam(':receiver_id', $prod_data['highest_bidder'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':sender_id', $_SESSION['userID'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':type', $item_deleted_notif, PDO::PARAM_STR, 16);
    $notify_last_bidder->bindParam(':about', $_SESSION['requestedProduct'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':extra', $prod_data['title'], PDO::PARAM_STR, 128);
    $notify_last_bidder->bindParam(':date_time', $date_time, PDO::PARAM_INT);

    try {
        $notify_last_bidder->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }
}

header ('Location: ../product.php');

?>