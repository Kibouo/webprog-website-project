<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get core data of the products sold by currently watched user
$prod_data_query = $connection->prepare('SELECT *
                                        FROM products
                                        WHERE seller = :reqUser');
$prod_data_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $prod_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_data_query->fetchAll(PDO::FETCH_ASSOC);
