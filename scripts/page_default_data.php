<?php

session_start();


// Init var for 1st visit on webpage
if (!isset($_SESSION['isLoggedIn'])) {
    $_SESSION['isLoggedIn'] = false;
}
if (!isset($_SESSION['userID'])) {
    $_SESSION['userID'] = -1;
}
if (!isset($_SESSION['userRole'])) {
    $_SESSION['userRole'] = 'visitor';
}

// If the user is logged in, get the amount of notifications he got
if ($_SESSION['isLoggedIn'])
{
    require_once 'scripts/get_amt_notifications.php';
}

?>
