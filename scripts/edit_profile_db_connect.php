<?php

function dataPrivacyChoiceConvert($choiceEmail, $choiceTelephone, $choiceLocation)
{
    $convertedChoices = 0;

    if ($choiceEmail == 1) {
        $convertedChoices += 4;
    }
    if ($choiceTelephone == 1) {
        $convertedChoices += 2;
    }
    if ($choiceLocation == 1) {
        $convertedChoices += 1;
    }

    return $convertedChoices;
}

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = ' . DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// If picture was provided, upload it to the correct dir and set it as avatar
if (!empty($_FILES['user_picture']['name'])) {
    $_SESSION['edit_profile_update_avatar'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check file type
    $correct_type = false;
    if ($_FILES['user_picture']['type'] == 'image/jpg' || $_FILES['user_picture']['type'] == 'image/jpeg' || $_FILES['user_picture']['type'] == 'image/png' || $_FILES['user_picture']['type'] == 'image/gif') {
        $correct_type = true;
    }

    // Check file size <2MB (default in php.ini)
    $correct_size = false;
    if ($_FILES['user_picture']['size'] < 2000000) {
        $correct_size = true;
    }

    // Check for upload errors
    $file_upload_error = true;
    if ($_FILES['user_profile']['error'] == 0) {
        $file_upload_error = false;
    }

    // Check if filename not too long to prevent error when placing in DB
    $correct_filename_len = false;
    if (strlen($_FILES['user_profile']['name']) < 100) {
        $correct_filename_len = true;
    }

    $upload_dir = '../img/upload/avatar/';
    $upload_file = $upload_dir . basename($_FILES['user_picture']['name']);


    // Move file from tmp upload folder to correct folder in case of no problems
    if (!$correct_filename_len || !$correct_size || !$correct_type || $file_upload_error || !move_uploaded_file($_FILES['user_picture']['tmp_name'], $upload_file)) {
        header ('Location: ../edit_profile.php');
        print "\nError: file upload error";
        die();
    }

    // Set the picture as avatar
    $update_avatar = $connection->prepare('UPDATE user_data
                                            SET avatar = :avatar
                                            WHERE id = :userID');
    $avatar = 'img/upload/avatar/' . basename($_FILES['user_picture']['name']);
    $update_avatar->bindParam(':avatar', $avatar, PDO::PARAM_STR, 128);
    if ($_SESSION['userRole'] == "admin") {
        $update_avatar->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
    } else {
        $update_avatar->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
    }

    try {
        $update_avatar->execute();
    } catch (PDOException $PDOException) {
        header ('Location: ../edit_profile.php');
        print "\nError: " . $PDOException->getMessage();
        die();
    }
    $_SESSION['edit_profile_update_avatar'] = true;
    $_SESSION['edit_profile_successful'] = true;
}


// If username was provided, change the user's data
if (!empty($_REQUEST['user_name'])) {
    $_SESSION['edit_profile_update_username'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check username length
    $username_format = false;
    if (strlen($_REQUEST['user_name']) >= 4 && strlen($_REQUEST['user_name']) <= 64) {
        $username_format = true;
    }

    if ($username_format) {
        $update_username = $connection->prepare('UPDATE user_data
                                                    SET username = :username
                                                    WHERE id = :userID');
        $update_username->bindParam(':username', $_REQUEST['user_name'], PDO::PARAM_STR, 64);
        if ($_SESSION['userRole'] == "admin") {
            $update_username->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_username->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_username->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_username'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}


// If password was provided, change the user's data
if (!empty($_REQUEST['user_old_pass']) || !empty($_REQUEST['user_new_pass']) || !empty($_REQUEST['user_new_pass_repeat'])) {
    $_SESSION['edit_profile_update_password'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check all password fields filled in
    $all_pass_fields_filled = false;
    if (!empty($_REQUEST['user_old_pass']) && !empty($_REQUEST['user_new_pass']) && !empty($_REQUEST['user_new_pass_repeat'])) {
        $all_pass_fields_filled = true;
    }

    // Check old_pass is correct
    $old_pass_correct = false;

    $get_salt = $connection->prepare('SELECT salt
                                        FROM users
                                        WHERE id = :id');
    if ($_SESSION['userRole'] == "admin") {
        $get_salt->bindParam(':id', $_SESSION['requestedUser'], PDO::PARAM_INT);
    } else {
        $get_salt->bindParam(':id', $_SESSION['userID'], PDO::PARAM_INT);
    }

    try {
        $get_salt->execute();
    } catch (PDOException $PDOException) {
        header('Location: ../edit_profile.php');
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    // Add salt & pepper to password
    $tmp_salt = $get_salt->fetch(PDO::FETCH_ASSOC);
    $salt = $tmp_salt['salt'];
    $pepper = '95d6773f5da96fbeb3b9bc0d0a368fea5098349df9ec5e5dea3692a21ba5e3cb33acdc5e758a25c5a400002e4e436cb8ce5187316d08684ab78d80db5abc6e87';
    $salted_old_pass = $salt . $_REQUEST['user_old_pass'] . $pepper;

    // Hash password
    $hashed_old_pass = hash('sha512', $salted_old_pass);

    // Check if hashed old_pass = user_password
    $check_old_pass = $connection->prepare('SELECT id
                                                FROM users
                                                WHERE password = :password AND id = :id');
    $check_old_pass->bindParam(':password', $hashed_old_pass, PDO::PARAM_STR, 128);
    if ($_SESSION['userRole'] == "admin") {
        $check_old_pass->bindParam(':id', $_SESSION['requestedUser'], PDO::PARAM_INT);
    } else {
        $check_old_pass->bindParam(':id', $_SESSION['userID'], PDO::PARAM_INT);
    }

    try {
        $check_old_pass->execute();
    } catch (PDOException $PDOException) {
        header('Location: ../edit_profile.php');
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $tmp_old_pass = $check_old_pass->fetch(PDO::FETCH_ASSOC);
    if (empty($tmp_old_pass['id'])) {
        header('Location: ../edit_profile.php');
        print "\nError: old password was incorrect.";
        die();
    } else {
         $old_pass_correct = true;
    }

    // Check if new passwords are the same
    $new_pass_same = false;
    if ($_REQUEST['user_new_pass'] == $_REQUEST['user_new_pass_repeat']) {
        $new_pass_same = true;
    }

    // Check password length
    $new_pass_len = false;
    if (strlen($_REQUEST['user_new_pass'] ) >= 8 && strlen($_REQUEST['user_new_pass'] ) <= 72) {
        $new_pass_len = true;
    }


    // If password checks are ok, edit user pass
    if ($new_pass_same && $all_pass_fields_filled && $old_pass_correct && $new_pass_len) {
        // Add salt & pepper to password
        //php5 = rand; php7 = random_int
        $salt = hash('sha512', rand(0, 32768));
        $salted_password = $salt . $_REQUEST['user_new_pass'] . $pepper;

        // Hash password
        $hashed_password = hash('sha512', $salted_password);

        // Update password & salt
        $update_pass_salt = $connection->prepare('UPDATE users
                                                    SET password = :password, salt = :salt
                                                    WHERE id = :id');
        $update_pass_salt->bindParam(':password', $hashed_password, PDO::PARAM_STR, 128);
        $update_pass_salt->bindParam(':salt', $salt, PDO::PARAM_STR, 128);
        if ($_SESSION['userRole'] == "admin") {
            $update_pass_salt->bindParam(':id', $_SESSION['userID'], PDO::PARAM_INT);
        } else {
            $update_pass_salt->bindParam(':id', $_SESSION['requestedUser'], PDO::PARAM_INT);
        }

        try {
            $update_pass_salt->execute();
        } catch (PDOException $PDOException) {
            header('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_password'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}

// If street was provided, change the user's data
if (!empty($_REQUEST['user_street'])) {
    $_SESSION['edit_profile_update_street'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check street length
    $street_format = false;
    if (strlen($_REQUEST['user_street']) >= 0 && strlen($_REQUEST['user_street']) <= 128) {
        $street_format = true;
    }

    if ($street_format) {
        $update_street = $connection->prepare('UPDATE user_data
                                                    SET street = :street
                                                    WHERE id = :userID');
        $update_street->bindParam(':street', $_REQUEST['user_street'], PDO::PARAM_STR, 128);
        if ($_SESSION['userRole'] == "admin") {
            $update_street->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_street->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_street->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_street'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}

// If houseNr. was provided, change the user's data
if (!empty($_REQUEST['user_housenr'])) {
    $_SESSION['edit_profile_update_housenr'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check houseNr
    $housenr_format = false;
    if (strlen($_REQUEST['user_housenr']) >= 0 && strlen($_REQUEST['user_housenr']) <= 8) {
        $housenr_format = true;
    }

    if ($housenr_format) {
        $update_housenr = $connection->prepare('UPDATE user_data
                                                    SET housenr = :housenr
                                                    WHERE id = :userID');
        $update_housenr->bindParam(':housenr', $_REQUEST['user_housenr'], PDO::PARAM_STR, 8);
        if ($_SESSION['userRole'] == "admin") {
            $update_housenr->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_housenr->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_housenr->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_housenr'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}

// If postal code was provided, change the user's data
if (!empty($_REQUEST['user_postal_code'])) {
    $_SESSION['edit_profile_update_postal_code'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check postal_code
    $postal_code_format = false;
    if (strlen($_REQUEST['user_postal_code']) >= 0 && strlen($_REQUEST['user_postal_code']) <= 16) {
        $postal_code_format = true;
    }

    if ($postal_code_format) {
        $update_postal_code = $connection->prepare('UPDATE user_data
                                                    SET postcode = :postal_code
                                                    WHERE id = :userID');
        $update_postal_code->bindParam(':postal_code', $_REQUEST['user_postal_code'], PDO::PARAM_STR, 16);
        if ($_SESSION['userRole'] == "admin") {
            $update_postal_code->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_postal_code->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_postal_code->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_postal_code'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}

// If country was provided, change the user's data
if (!empty($_REQUEST['user_country'])) {
    $_SESSION['edit_profile_update_country'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check country
    $country_format = false;
    if (strlen($_REQUEST['user_country']) >= 0 && strlen($_REQUEST['user_country']) <= 64) {
        $country_format = true;
    }

    if ($country_format) {
        $update_country = $connection->prepare('UPDATE user_data
                                                    SET country = :country
                                                    WHERE id = :userID');
        $update_country->bindParam(':country', $_REQUEST['user_country'], PDO::PARAM_STR, 64);
        if ($_SESSION['userRole'] == "admin") {
            $update_country->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_country->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_country->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_country'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}

// If Tel.Nr. was provided, change the user's data
if (!empty($_REQUEST['user_tel_nr'])) {
    $_SESSION['edit_profile_update_tel_nr'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check tel_nr
    $tel_nr_format = false;
    if (strlen($_REQUEST['user_tel_nr']) >= 0 && strlen($_REQUEST['user_tel_nr']) <= 16) {
        $tel_nr_format = true;
    }

    if ($tel_nr_format) {
        $update_tel_nr = $connection->prepare('UPDATE user_data
                                                    SET telnr = :tel_nr
                                                    WHERE id = :userID');
        $update_tel_nr->bindParam(':tel_nr', $_REQUEST['user_tel_nr'], PDO::PARAM_STR, 16);
        if ($_SESSION['userRole'] == "admin") {
            $update_tel_nr->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_tel_nr->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_tel_nr->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_tel_nr'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}

// If description was provided, change the user's data
if (!empty($_REQUEST['user_description'])) {
    $_SESSION['edit_profile_update_description'] = false;
    $_SESSION['edit_profile_successful'] = false;

    // Check description
    $description_format = false;
    if (strlen($_REQUEST['user_description']) >= 0 && strlen($_REQUEST['user_description']) <= 512) {
        $description_format = true;
    }

    if ($description_format) {
        $update_description = $connection->prepare('UPDATE user_data
                                                    SET description = :description
                                                    WHERE id = :userID');
        $update_description->bindParam(':description', $_REQUEST['user_description'], PDO::PARAM_STR, 512);
        if ($_SESSION['userRole'] == "admin") {
            $update_description->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
        } else {
            $update_description->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
        }

        try {
            $update_description->execute();
        } catch (PDOException $PDOException) {
            header ('Location: ../edit_profile.php');
            print "\nError: " . $PDOException->getMessage();
            die();
        }
        $_SESSION['edit_profile_update_description'] = true;
        $_SESSION['edit_profile_successful'] = true;
    }
}


// If privacy info was updated, change the user's data
if ($_SESSION['showEmail'] != $_REQUEST['user_show_email'] || $_SESSION['showTelephone'] != $_REQUEST['user_show_telephone'] || $_SESSION['showLocation'] != $_REQUEST['user_show_location']) {
    $_SESSION['edit_profile_update_privacy'] = false;
    $_SESSION['edit_profile_successful'] = false;

    $convertedChoices = dataPrivacyChoiceConvert($_REQUEST['user_show_email'], $_REQUEST['user_show_telephone'], $_REQUEST['user_show_location']);

    $update_privacy_choices = $connection->prepare('UPDATE user_data
                                                    SET "dataPrivacy" = :privacyChoices
                                                    WHERE id = :userID');
    $update_privacy_choices->bindParam(':privacyChoices', $convertedChoices, PDO::PARAM_INT);
    if ($_SESSION['userRole'] == "admin") {
        $update_privacy_choices->bindParam(':userID', $_SESSION['requestedUser'], PDO::PARAM_INT);
    } else {
        $update_privacy_choices->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);
    }

    try {
        $update_privacy_choices->execute();
    } catch (PDOException $PDOException) {
        header ('Location: ../edit_profile.php');
        print "\nError: " . $PDOException->getMessage();
        die();
    }
    $_SESSION['edit_profile_update_privacy'] = true;
    $_SESSION['edit_profile_successful'] = true;
}

header('Location: ../edit_profile.php');

?>