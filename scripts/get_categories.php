<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get all categories present in the db
$categories_query = $connection->prepare('SELECT "category"
                                            FROM categories');

try {
    $categories_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$categories = $categories_query->fetchAll(PDO::FETCH_ASSOC);

?>