<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get all unread notifications which were sent to the current player
$notifs_are_read = false;

$unread_notifs_query = $connection->prepare('SELECT *
                                            FROM notifications
                                            WHERE receiver_id = :current_player AND read = :read
                                            ORDER BY date_time DESC');
$unread_notifs_query->bindParam(':current_player', $_SESSION['userID'], PDO::PARAM_INT);
$unread_notifs_query->bindParam(':read', $notifs_are_read, PDO::PARAM_BOOL);

try {
    $unread_notifs_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$unread_notifications = $unread_notifs_query->fetchAll(PDO::FETCH_ASSOC);


// Get all read notifications which were sent to the current player
$notifs_are_read = true;

$read_notifs_query = $connection->prepare('SELECT *
                                            FROM notifications
                                            WHERE receiver_id = :current_player AND read = :read
                                            ORDER BY date_time DESC');
$read_notifs_query->bindParam(':current_player', $_SESSION['userID'], PDO::PARAM_INT);
$read_notifs_query->bindParam(':read', $notifs_are_read, PDO::PARAM_BOOL);

try {
    $read_notifs_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$read_notifications = $read_notifs_query->fetchAll(PDO::FETCH_ASSOC);

// Update all notifications sent to current user as read
$notifs_are_read = false;
$unread_are_now_read = true;

$unread_notifs_update_query = $connection->prepare('UPDATE notifications
                                                    SET read = :new_read
                                                    WHERE receiver_id = :current_player AND read = :read');
$unread_notifs_update_query->bindParam(':new_read', $unread_are_now_read, PDO::PARAM_BOOL);
$unread_notifs_update_query->bindParam(':current_player', $_SESSION['userID'], PDO::PARAM_INT);
$unread_notifs_update_query->bindParam(':read', $notifs_are_read, PDO::PARAM_BOOL);

try {
    $unread_notifs_update_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

?>