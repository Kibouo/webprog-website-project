// AJAX request prepare
var xmlhttp = new XMLHttpRequest();


// This function handles the response of the AJAX request
function LoadNewProducts()
{
    if ((xmlhttp) && (xmlhttp.readyState == 4) && (xmlhttp.status == 200))
    {
        var div = document.getElementById('page_content');
        div.innerHTML = div.innerHTML + xmlhttp.responseText;
    }
}


// When we scroll to the bottom of the page (with buffer of 100px), we load extra products
document.onscroll = function() {
    if(document.documentElement.scrollTop + window.innerHeight >= document.documentElement.scrollHeight - 100)
    {
        // AJAX call to get product data from server and append to the div id="page_content".
        xmlhttp.onreadystatechange = LoadNewProducts();
        xmlhttp.open("GET", "scripts/get_additional_products.php", true);
        xmlhttp.send(null);
    }
}