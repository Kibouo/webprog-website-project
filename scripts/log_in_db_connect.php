<?php

// TODO: anti-bruteforce

session_start();


require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = ' . DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Check required input is present
$elements_present = false;
$array_elem_count = 0;
foreach ($_REQUEST as $element) {
    $array_elem_count++;
}

if ($array_elem_count == 3 && $_REQUEST['submit'] == 'Log in') {
    $elements_present = true;
}


// Check if e-mail matches requirements
$correct_email_format = false;
if (filter_var($_REQUEST['user_email'], FILTER_VALIDATE_EMAIL)) {
    $length_email = strlen($_REQUEST['user_email']);
    if ($length_email > 0 && $length_email <= 64) {
        $correct_email_format = true;
    }
}

// Check if password matches requirements
$correct_pass_len = false;
$length_password = strlen($_REQUEST['user_pass']);
if ($length_password >= 8 && $length_password <= 72) {
    $correct_pass_len = true;
}


// Input format check
if (!$correct_email_format || !$elements_present || !$correct_pass_len) {
    header('Location: ../log_in.php');
    $_SESSION["log_inRequirementsAccepted"] = false;
    print "\nError: some requirements were not met.";
    die();
}


// Check if user exists
$user_exists = $connection->prepare('SELECT email
                                        FROM users
                                        WHERE email = :email');
$user_exists->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);

try {
    $user_exists->execute();
} catch (PDOException $PDOException) {
    header('Location: ../log_in.php');
    $_SESSION['log_inUsername_passwordCorrect'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}

$tmp_user_exists = $user_exists->fetch(PDO::FETCH_ASSOC);
if (empty($tmp_user_exists['email'])) {
    header('Location: ../log_in.php');
    $_SESSION['log_inUserExists'] = false;
    print "\nError: this user doesn't exist.";
    die();
}

// Get wanted users' salt
$get_salt = $connection->prepare('SELECT salt
                                    FROM users
                                    WHERE email = :email');
$get_salt->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);

try {
    $get_salt->execute();
} catch (PDOException $PDOException) {
    header('Location: ../log_in.php');
    $_SESSION['log_inUsername_passwordCorrect'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Add salt & pepper to password
$tmp_get_salt = $get_salt->fetch(PDO::FETCH_ASSOC);
$salt = $tmp_get_salt['salt'];
$pepper = '95d6773f5da96fbeb3b9bc0d0a368fea5098349df9ec5e5dea3692a21ba5e3cb33acdc5e758a25c5a400002e4e436cb8ce5187316d08684ab78d80db5abc6e87';
$salted_password = $salt . $_REQUEST['user_pass'] . $pepper;

// Hash password
$hashed_password = hash('sha512', $salted_password);


// Get user core info
$log_in = $connection->prepare('SELECT id, role
                                FROM users
                                WHERE password = :password AND email = :email');
$log_in->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);
$log_in->bindParam(':password', $hashed_password, PDO::PARAM_STR, 128);

try {
    $log_in->execute();
} catch (PDOException $PDOException) {
    header('Location: ../log_in.php');
    $_SESSION['log_inUsername_passwordCorrect'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Check if email-password pair was correct
$user_info = $log_in->fetch(PDO::FETCH_ASSOC);
$user_id = $user_info['id'];
$user_role = $user_info['role'];

if (empty($user_id) || empty($user_role)) {
    header('Location: ../log_in.php');
    $_SESSION['log_inUsername_passwordCorrect'] = false;
    print "\nError: the provided username or password is incorrect.";
    die();
}


// Set logged in session info
$_SESSION['isLoggedIn'] = true;
$_SESSION['userID'] = $user_id;
$_SESSION['userRole'] = $user_role;

header('Location: ../index.php');

?>