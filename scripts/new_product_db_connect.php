<?php

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = ' . DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// This var will be true if we reach the end of this file, and thus haven't encountered any problems while updating user data.
$_SESSION['list_new_product_success'] = false;

// Var to inform user about failure in formatting.
$_SESSION['product_data_format'] = false;

// Check product title formatting
$prodTitleFormat = false;
if (!empty($_REQUEST['product_title']) && strlen($_REQUEST['product_title']) >= 4 && strlen($_REQUEST['product_title']) <= 128) {
    $prodTitleFormat = true;
}

// Check product price formatting
$prodPriceFormat = false;
if (!empty($_REQUEST['product_price']) && $_REQUEST['product_price'] > 0 && is_numeric($_REQUEST['product_price'])) {
    $prodPriceFormat = true;
}

// Check category exists in db
$prodCategoryFormat = false;
if (!empty($_REQUEST['product_category'])) {
    $category_exists_query = $connection->prepare('SELECT *
                                                    FROM categories
                                                    WHERE category = :category');
    $category_exists_query->bindParam(':category', $_REQUEST['product_category'], PDO::PARAM_STR, 64);

    try {
        $category_exists_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $possible_category = $category_exists_query->fetch(PDO::FETCH_ASSOC);
    
    if ($_REQUEST['product_category'] == $possible_category['category']) {
        $prodCategoryFormat = true;
    }
}

// Check product descr formatting
$prodDescrFormat = false;
if (!empty($_REQUEST['product_description']) && strlen($_REQUEST['product_description']) > 0 && strlen($_REQUEST['product_description']) <= 512) {
    $prodDescrFormat = true;
}

// If everything was provided properly
if (!empty($_FILES['product_picture']['name']) && $prodTitleFormat && $prodPriceFormat && $prodCategoryFormat && $prodDescrFormat) {
    // Generic data provided was correctly formatted
    $_SESSION['product_data_format'] = true;

    // Var to keep track of files being uploaded successfully
    $_SESSION['product_media_uploaded'] = false;

    // Check file type
    $correct_type = false;
    if ($_FILES['product_picture']['type'] == 'image/jpg' || $_FILES['product_picture']['type'] == 'image/jpeg' || $_FILES['product_picture']['type'] == 'image/png' || $_FILES['product_picture']['type'] == 'image/gif') {
        $correct_type = true;
    }
    
    // Check file size <2MB (default in php.ini)
    $correct_size = false;
    if ($_FILES['product_picture']['size'] < 2000000) {
        $correct_size = true;
    }

    // Check for upload errors
    $file_upload_error = true;
    if ($_FILES['product_picture']['error'] == 0) {
        $file_upload_error = false;
    }

    // Check if filename not too long to prevent error when placing in DB
    $correct_filename_len = false;
    if (strlen($_FILES['product_picture']['name']) < 100) {
        $correct_filename_len = true;
    }

    $upload_dir = '../img/upload/product/';
    $upload_file = $upload_dir . basename($_FILES['product_picture']['name']);

    // Move file from tmp upload folder to correct folder, or die if something went wrong
    if (!$correct_filename_len || !$correct_size || !$correct_type || $file_upload_error || !move_uploaded_file($_FILES['product_picture']['tmp_name'], $upload_file)) {
        header ('Location: ../new_product.php');
        print "\nError: product picture file upload error";
        die();
    }

    // Everything is present, now place the product as an entry in the db
    $date = time();

    $insert_prod_query = $connection->prepare('INSERT INTO products(price, seller, category, date_time, title, picture, descr)
                                                VALUES (:price, :seller, :category, :date_time, :title, :picture, :descr)');
    $insert_prod_query->bindParam(':price', $_REQUEST['product_price'], PDO::PARAM_INT);
    $insert_prod_query->bindParam(':seller', $_SESSION['userID'], PDO::PARAM_INT);
    $insert_prod_query->bindParam(':category', $_REQUEST['product_category'], PDO::PARAM_STR, 64);
    $insert_prod_query->bindParam(':date_time', $date, PDO::PARAM_INT);
    $insert_prod_query->bindParam(':title', $_REQUEST['product_title'], PDO::PARAM_STR, 128);
    $picture = 'img/upload/product/' . basename($_FILES['product_picture']['name']);
    $insert_prod_query->bindParam(':picture', $picture, PDO::PARAM_STR, 128);
    $insert_prod_query->bindParam(':descr', $_REQUEST['product_description'], PDO::PARAM_STR, 64);

    try {
        $insert_prod_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    // Get new product's id
    $possible_rows = $connection->prepare('SELECT id
                                        FROM products
                                        WHERE price = :price AND seller = :seller AND category = :category');
    $possible_rows->bindParam(':price', $_REQUEST['product_price'], PDO::PARAM_INT);
    $possible_rows->bindParam(':seller', $_SESSION['userID'], PDO::PARAM_INT);
    $possible_rows->bindParam(':category', $_REQUEST['product_category'], PDO::PARAM_STR, 64);
    
    try {
        $possible_rows->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    // Just in case there are multiple products from this seller with the same price and same category, we get the last item in the db (which is the newest).
    $id = end($possible_rows->fetchAll(PDO::FETCH_ASSOC))['id'];

    if (empty($id)) {
        header ('Location: ../new_product.php');
        print "\nError: something went wrong while trying to register the new product.";
        die();
    }
    $_SESSION['requestedProduct'] = $id;

    $extra_media_upload_success = true;

    // If there was additional media provided, upload it as well
    $i = 0;
    while (!empty($_FILES['product_media']['name'][$i])) {
        $extra_media_upload_success = false;

        // Check file type
        $correct_type = false;
        if ($_FILES['product_media']['type'][$i] == 'image/jpg' || $_FILES['product_media']['type'][$i] == 'image/jpeg' || $_FILES['product_media']['type'][$i] == 'image/png' || $_FILES['product_media']['type'][$i] == 'image/gif' || $_FILES['product_media']['type'][$i] == 'video/mp4') {
            $correct_type = true;
        }

        // Check file size <2MB (default in php.ini)
        $correct_size = false;
        if ($_FILES['product_media']['size'][$i] < 2000000) {
            $correct_size = true;
        }

        // Check for upload errors
        $file_upload_error = true;
        if ($_FILES['product_media']['error'][$i] == 0) {
            $file_upload_error = false;
        }

        // Check if filename not too long to prevent error when placing in DB
        $correct_filename_len = false;
        if (strlen($_FILES['product_media']['name'][$i]) < 100) {
            $correct_filename_len = true;
        }

        $upload_dir = '../img/upload/product/';
        $upload_file = $upload_dir . basename($_FILES['product_media']['name'][$i]);
        
        // Move file from tmp upload folder to correct folder, or die if something went wrong
        if (!$correct_filename_len || !$correct_size || !$correct_type || $file_upload_error || !move_uploaded_file($_FILES['product_media']['tmp_name'][$i], $upload_file)) {
            header ('Location: ../new_product.php');
            print "\nError: product picture file upload error";
            die();
        }

        // Media seems fine, add it to db
        $insert_media_query = $connection->prepare('INSERT INTO prod_media(id, link)
                                            VALUES (:id, :link)');
        $insert_media_query->bindParam(':id', $id, PDO::PARAM_INT);
        $insert_media_query->bindParam(':link', substr($upload_file, 3), PDO::PARAM_STR, 128);

        try {
            $insert_media_query->execute();
        } catch (PDOException $PDOException) {
            print "\nError: " . $PDOException->getMessage();
            die();
        }

        $extra_media_upload_success = true;
        // Try eval next media
        $i++;
    }

    if ($extra_media_upload_success) {
        $_SESSION['product_media_uploaded'] = true;
    }
    $_SESSION['list_new_product_success'] = true;
}

header('Location: ../new_product.php');

?>