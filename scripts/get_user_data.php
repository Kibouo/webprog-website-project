<?php

// Convert the number to binary, with each bit representing a different privacy choice flag.
function dataPrivacyParser($dataPrivacy)
{
    $showEmail = floor($dataPrivacy/ 4);
    $dataPrivacy %= 4;
    $showTelephone = floor($dataPrivacy/ 2);
    $dataPrivacy %= 2;
    $showLocation = floor($dataPrivacy/ 1);
    $dataPrivacy %= 1;

    $indvPrivacy = array(
                    0 => $showEmail,
                    1 => $showTelephone,
                    2 => $showLocation);
    return $indvPrivacy;
}

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Get rating of the requestedUser
$user_rating_query = $connection->prepare('SELECT id, score
                                            FROM user_scores
                                            WHERE id = :reqUser');
$user_rating_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $user_rating_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Get data of the requestedUser
$user_data_query = $connection->prepare('SELECT *
                                            FROM user_data
                                            WHERE id = :reqUser');
$user_data_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $user_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Get e-mail of the requestedUser
$user_email_query = $connection->prepare('SELECT email
                                            FROM users
                                            WHERE id = :reqUser');
$user_email_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $user_email_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Check if the requested user had data and thus exists
$reqUserFound = false;
$user_data = $user_data_query->fetch(PDO::FETCH_ASSOC);
$user_email = $user_email_query->fetch(PDO::FETCH_ASSOC);
$user_rating = $user_rating_query->fetch(PDO::FETCH_ASSOC);

if (!empty($user_data['id']) && !empty($user_email['email']) && !empty($user_rating['id'])) {
    $reqUserFound = true;

    // User exists, now put data in vars to display on page
    $avatar = $user_data['avatar'];
    $username = $user_data['username'];
    $email = $user_email['email'];
    $telnr = $user_data['telnr'];
    $country = $user_data['country'];
    $postcode = $user_data['postcode'];
    $housenr = $user_data['housenr'];
    $street = $user_data['street'];
    $description = $user_data['description'];

    $indvPrivacy = dataPrivacyParser($user_data['dataPrivacy']);
    $_SESSION['showEmail'] = $indvPrivacy[0];
    $_SESSION['showTelephone'] = $indvPrivacy[1];
    $_SESSION['showLocation'] = $indvPrivacy[2];

    $reqUserRating = round($user_rating['score'], 0);
}

?>