<?php

// Convert the number to binary, with each bit representing a different privacy choice flag.
function dataPrivacyParser($dataPrivacy)
{
    $showEmail = floor($dataPrivacy/ 4);
    $dataPrivacy %= 4;
    $showTelephone = floor($dataPrivacy/ 2);
    $dataPrivacy %= 2;
    $showLocation = floor($dataPrivacy/ 1);
    $dataPrivacy %= 1;

    $indvPrivacy = array(
                    0 => $showEmail,
                    1 => $showTelephone,
                    2 => $showLocation);
    return $indvPrivacy;
}

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get data of the requestedUser
$user_privacy_choices_query = $connection->prepare('SELECT "dataPrivacy"
                                                    FROM user_data
                                                    WHERE id = :reqUser');
$user_privacy_choices_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $user_privacy_choices_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$user_privacy_choices = $user_privacy_choices_query->fetch(PDO::FETCH_ASSOC);

$indvPrivacy = dataPrivacyParser($user_privacy_choices['dataPrivacy']);
$_SESSION['showEmail'] = $indvPrivacy[0];
$_SESSION['showTelephone'] = $indvPrivacy[1];
$_SESSION['showLocation'] = $indvPrivacy[2];

?>