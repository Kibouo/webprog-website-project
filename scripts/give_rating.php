<?php

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

require_once 'test_allowed_rate.php';

$_SESSION['ratingAccepted'] = false;

// Get rating of the requestedUser
$user_rating_query = $connection->prepare('SELECT amt_votes, score
                                            FROM user_scores
                                            WHERE id = :reqUser');
$user_rating_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $user_rating_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$rating = $user_rating_query->fetch(PDO::FETCH_ASSOC);


if ($allowed_to_rate)
{
    $amt_votes = $rating['amt_votes'] + 1;
    $score = ($rating['amt_votes'] * $rating['score'] + $_REQUEST['rating_given'])/$amt_votes;

    $update_rating_query = $connection->prepare('UPDATE user_scores
                                        SET amt_votes = :amt_votes, score = :score
                                        WHERE id = :reqUser');
    $update_rating_query->bindParam(':amt_votes', $amt_votes, PDO::PARAM_INT);
    $update_rating_query->bindParam(':score', $score, PDO::PARAM_INT);
    $update_rating_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);

    try {
        $update_rating_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }


    // Delete the 'allowed to rate' flag
    $delete_rate_flag_query = $connection->prepare('DELETE FROM rating_flag
                                                    WHERE sender_id = :this_id AND receiver_id = :reqUser');
    $delete_rate_flag_query->bindParam(':this_id', $_SESSION['userID'], PDO::PARAM_INT);
    $delete_rate_flag_query->bindParam(':reqUser', $_SESSION['requestedUser'], PDO::PARAM_INT);
    try {
        $delete_rate_flag_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }


    $_SESSION['ratingAccepted'] = true;
}

header ('Location: ../user.php');

?>