<?php

header('Location: ../index.php');

session_start();

// Reset logged in info to logged off/visitor state
$_SESSION['isLoggedIn'] = false;
$_SESSION['userID'] = -1;
$_SESSION['userRole'] = 'visitor';

?>