<?php

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get data of all products
$prod_data_query = $connection->prepare('SELECT *
                                        FROM products');
try {
    $prod_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_data_query->fetchAll(PDO::FETCH_ASSOC);

$products_array = new ArrayObject();
$_SESSION['amt_index_products'] += 5;
$products_string = "";


// Get 5 last edited products
for ($i = $_SESSION['amt_index_products'] - 5; $i < $_SESSION['amt_index_products']; $i++)
{
    // If there are still products found in the db
    if (isset($prod_data[$i]))
    {
        $products_array->append($prod_data[$i]);
    }
}


// make a string of html, which will be returned for js to use (append to main page).
for ($i = 0; $i < sizeof($products_array); $i++)
{
    $tmp_price = number_format($products_array[$i]['price'], 2, ",", " ");
    $tmp_title = wordwrap($products_array[$i]['title'], 24, "\n", true);
    $tmp_category = urlencode($products_array[$i]['category']);
    $products_string = $products_string . '
    <a class="item_link" href="scripts/set_requested_product.php?product_id=' . $products_array[$i]['id'] . '">
        <div class="filtered_product">
            <img alt="Product picture" class="prod_picture" src="' . $products_array[$i]['picture'] . '">
            <h1 class="prod_title">' . $tmp_title . '</h1>
            <div class="price">Price: €' . $tmp_price . '</div>
            <div class="tags">Category: ' . $products_array[$i]['category'] . '</div>
        </div>
    </a>';
}

echo $products_string;

?>