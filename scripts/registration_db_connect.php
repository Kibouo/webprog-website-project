<?php

session_start();

require_once 'globals.php';

try
{
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = ' . DB_NAME, DB_USER, DB_PASS);
}
catch (PDOException $PDOException)
{
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Check required input is present
$elements_present = false;
$array_elem_count = 0;
foreach ($_REQUEST as $element)
{
    $array_elem_count++;
}

if ($array_elem_count == 4 && $_REQUEST['submit'] == 'Register!')
{
    $elements_present = true;
}


// Check if e-mail already exists
$existing_email_check = $connection->prepare('SELECT email
                                                FROM users
                                                WHERE email = :email');
$existing_email_check->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);

try
{
    $existing_email_check->execute();
}
catch (PDOException $PDOException)
{
    header('Location: ../registration.php');
    $_SESSION['registrationEmailAvailable'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}


$existing_email = $existing_email_check->fetch(PDO::FETCH_ASSOC);
if (!empty($existing_email['email']))
{
    header('Location: ../registration.php');
    $_SESSION['registrationEmailAvailable'] = false;
    print "\nError: e-mail already exists.";
    die();
}


// Check if e-mail matches requirements
$correct_email_format = false;
if (filter_var($_REQUEST['user_email'], FILTER_VALIDATE_EMAIL))
{
    $length_email = strlen($_REQUEST['user_email']);
    if ($length_email > 0 && $length_email <= 64)
    {
        $correct_email_format = true;
    }
}

// Check matching passwords
$matching_passwords = false;
if ($_REQUEST['user_pass'] == $_REQUEST['user_pass_repeat'])
{
   $matching_passwords = true; 
}

// Check if passwords match requirements
$correct_pass_len = false;
$length_password = strlen($_REQUEST['user_pass']);
if ($length_password >= 8 && $length_password <= 72)
{
    $correct_pass_len = true;
}


// Input format check
if (!$correct_email_format || !$elements_present || !$matching_passwords || !$correct_pass_len)
{
    header('Location: ../registration.php');
    $_SESSION['registrationRequirementsAccepted'] = false;
    print "\nError: some requirements were not met.";
    die();
}


// Add salt & pepper to password
//php5 = rand; php7 = random_int
$salt = hash('sha512', rand(0, 32768));
$pepper = '95d6773f5da96fbeb3b9bc0d0a368fea5098349df9ec5e5dea3692a21ba5e3cb33acdc5e758a25c5a400002e4e436cb8ce5187316d08684ab78d80db5abc6e87';
$salted_password = $salt . $_REQUEST['user_pass'] . $pepper;

// Hash password
$hashed_password = hash('sha512', $salted_password);


// Init new user in users
$user_init = $connection->prepare('INSERT INTO users(password, email, salt)
                                    VALUES (:password, :email, :salt)');
$user_init->bindParam(':password', $hashed_password, PDO::PARAM_STR, 128);
$user_init->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);
$user_init->bindParam(':salt', $salt, PDO::PARAM_STR, 128);

try
{
    $user_init->execute();
}
catch (PDOException $PDOException)
{
    header('Location: ../user.php');
    $_SESSION['registrationRequirementsAccepted'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Get new user's id
$added_row = $connection->prepare('SELECT id
                                    FROM users
                                    WHERE email = :email');
$added_row->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);
    
try
{
    $added_row->execute();
}
catch (PDOException $PDOException)
{
    header('Location: ../user.php');
    $_SESSION['registrationRequirementsAccepted'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}
        
$tmp_found_id = $added_row->fetch(PDO::FETCH_ASSOC);
$new_id = $tmp_found_id['id'];


// Init row for new user in user_data
$user_data_init = $connection->prepare('INSERT INTO user_data(username, id)
                                        VALUES (:username, :id)');
$user_data_init->bindParam(':username', $_REQUEST['user_email'], PDO::PARAM_STR, 64);
$user_data_init->bindParam(':id', $new_id, PDO::PARAM_INT);
        
try
{
    $user_data_init->execute();
}
catch (PDOException $PDOException)
{
    header('Location: ../user.php');
    $_SESSION['registrationRequirementsAccepted'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Init row for new user in user_scores
$user_scores_init = $connection->prepare('INSERT INTO user_scores(id)
                                            VALUES (:id)');
$user_scores_init->bindParam(':id', $new_id, PDO::PARAM_INT);

try
{
    $user_scores_init->execute();
}
catch (PDOException $PDOException)
{
    header('Location: ../user.php');
    $_SESSION['registrationRequirementsAccepted'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Get user core info
$log_in = $connection->prepare('SELECT id, role
                                FROM users
                                WHERE password = :password AND email = :email');
$log_in->bindParam(':password', $hashed_password, PDO::PARAM_STR, 128);
$log_in->bindParam(':email', $_REQUEST['user_email'], PDO::PARAM_STR, 64);

try
{
    $log_in->execute();
}
catch (PDOException $PDOException)
{
    header('Location: ../user.php');
    $_SESSION['registrationRequirementsAccepted'] = false;
    print "\nError: " . $PDOException->getMessage();
    die();
}

$tmp_user_data = $log_in->fetch(PDO::FETCH_ASSOC);
$user_id = $tmp_user_data['id'];
$user_role = $tmp_user_data['role'];


// Set logged in session info
$_SESSION['isLoggedIn'] = true;
$_SESSION['userID'] = $user_id;
$_SESSION['userRole'] = $user_role;

// Set user ID for userpage
$_SESSION['requestedUser'] = $user_id;
header('Location: ../user.php');
  
?>