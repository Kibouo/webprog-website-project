<?php

session_start();

// The string which we search for is only updated if the textbox in the header is used. This enables us to first search on a string provided,
//and then make adjustments without having to force the user to type the search term again.
$_SESSION['search_string'] = $_REQUEST['search_string'];

header ('Location: ../product_filtering.php');

?>