<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get all products of the requested category
$prod_data_query = $connection->prepare('SELECT *
                                    FROM products
                                    WHERE category = :reqCategory
                                    ORDER BY date_time DESC');
$prod_data_query->bindParam(':reqCategory', $_SESSION['requestedCategory'], PDO::PARAM_STR, 64);

try {
    $prod_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_data_query->fetchAll(PDO::FETCH_ASSOC);

?>