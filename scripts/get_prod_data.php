<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get core data of the requested product
$prod_data_query = $connection->prepare('SELECT *
                                                FROM products
                                                WHERE id = :reqId');
$prod_data_query->bindParam(':reqId', $_SESSION['requestedProduct'], PDO::PARAM_INT);

try {
    $prod_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_data_query->fetch(PDO::FETCH_ASSOC);


// Get media data of the requested product
$prod_media_data_query = $connection->prepare('SELECT *
                                                FROM prod_media
                                                WHERE id = :reqId');
$prod_media_data_query->bindParam(':reqId', $_SESSION['requestedProduct'], PDO::PARAM_INT);

try {
    $prod_media_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_media_data = $prod_media_data_query->fetchAll(PDO::FETCH_ASSOC);


// Get username of seller
$seller_username_query = $connection->prepare('SELECT username
                                            FROM user_data
                                            WHERE id = :seller');
$seller_username_query->bindParam(':seller', $prod_data['seller'], PDO::PARAM_INT);

try {
    $seller_username_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$seller_username = $seller_username_query->fetch(PDO::FETCH_ASSOC);

?>