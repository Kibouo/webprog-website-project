<?php

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$_SESSION['deletedUser'] = false;

// If requesting user is admin (so no illegal access to this script), delete the requested user
if ($_SESSION['userRole'] == "admin") {
    $delete_user_query = $connection->prepare('DELETE FROM users
                                                WHERE id = :id');
    $delete_user_query->bindParam(':id', $_SESSION['requestedUser'], PDO::PARAM_INT);

    try {
        $delete_user_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $_SESSION['deletedUser'] = true;
}

header ('Location: ../user.php');

?>