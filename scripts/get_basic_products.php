<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Get data of all products
$prod_data_query = $connection->prepare('SELECT *
                                        FROM products');
try {
    $prod_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_data_query->fetchAll(PDO::FETCH_ASSOC);

$_SESSION['amt_index_products'] += 5;

// Get 5 last edited products
for ($i = $_SESSION['amt_index_products'] - 5; $i < $_SESSION['amt_index_products']; $i++)
{
    if (isset($prod_data[$i]))
    {
        $products_array->append($prod_data[$i]);
    }
}

?>