<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Get the flag which says whether the current user is allowed to rate the user being watched
$get_flag_query = $connection->prepare('SELECT done
                                        FROM rating_flag
                                        WHERE sender_id = :curr_user AND receiver_id = :user_being_watched');
$get_flag_query->bindParam(':curr_user', $_SESSION['userID'], PDO::PARAM_INT);
$get_flag_query->bindParam(':user_being_watched', $_SESSION['requestedUser'], PDO::PARAM_INT);

try {
    $get_flag_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$already_rated = $get_flag_query->fetch(PDO::FETCH_ASSOC);

// If there is no entry, it means you're not allowed to rate this user
if (is_null($already_rated['done']))
{
    $allowed_to_rate = false;
} else
{
    // The db saves whether the rating has been done. So if rating has not been done, it is allowed to rate
    $allowed_to_rate = !$already_rated['done'];
}

?>