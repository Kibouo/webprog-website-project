<?php

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


$_SESSION['bidAccepted'] = false;

// Get price of the requested product. Also get the highest bidder and seller already, in case we will have to send them a notification.
//This way we reduce the amount of db access.
$prod_query = $connection->prepare('SELECT price, highest_bidder, seller
                                            FROM products
                                            WHERE id = :reqId');
$prod_query->bindParam(':reqId', $_SESSION['requestedProduct'], PDO::PARAM_INT);

try {
    $prod_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_query->fetch(PDO::FETCH_ASSOC);
$prod_price = $prod_data['price'];


// Check product price formatting & whether it's more than the last highest bid.
if (!empty($_REQUEST['bid_price']) && $_REQUEST['bid_price'] > 0 && is_numeric($_REQUEST['bid_price']) && $_REQUEST['bid_price'] > $prod_price['price']) {
    $update_prod_query = $connection->prepare('UPDATE products
                                        SET price = :price, highest_bidder = :highest_bidder
                                        WHERE id = :reqId');
    $update_prod_query->bindParam(':price', $_REQUEST['bid_price'], PDO::PARAM_INT);
    $update_prod_query->bindParam(':highest_bidder', $_SESSION['userID'], PDO::PARAM_INT);
    $update_prod_query->bindParam(':reqId', $_SESSION['requestedProduct'], PDO::PARAM_INT);

    try {
        $update_prod_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $_SESSION['bidAccepted'] = true;
}


// If the current bidder placed a higher bid, notify last highest bidder and seller of item
if ($_SESSION['bidAccepted'])
{
    $date_time = time();

    // Notify seller of new bidding
    $new_bidding_notif = 'new_bidding';

    $notify_seller = $connection->prepare('INSERT INTO notifications(receiver_id, sender_id, type, about, date_time)
                                            VALUES (:receiver_id, :sender_id, :type, :about, :date_time)');
    $notify_seller->bindParam(':receiver_id', $prod_data['seller'], PDO::PARAM_INT);
    $notify_seller->bindParam(':sender_id', $_SESSION['userID'], PDO::PARAM_INT);
    $notify_seller->bindParam(':type', $new_bidding_notif, PDO::PARAM_STR, 16);
    $notify_seller->bindParam(':about', $_SESSION['requestedProduct'], PDO::PARAM_INT);
    $notify_seller->bindParam(':date_time', $date_time, PDO::PARAM_INT);

    try {
        $notify_seller->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    // Notify last highest bidder of being overbid
    $being_overbid_notif = 'overbid';

    $notify_last_bidder = $connection->prepare('INSERT INTO notifications(receiver_id, sender_id, type, about, date_time)
                                            VALUES (:receiver_id, :sender_id, :type, :about, :date_time) ');
    $notify_last_bidder->bindParam(':receiver_id', $prod_data['highest_bidder'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':sender_id', $_SESSION['userID'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':type', $being_overbid_notif, PDO::PARAM_STR, 16);
    $notify_last_bidder->bindParam(':about', $_SESSION['requestedProduct'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':date_time', $date_time, PDO::PARAM_INT);

    try {
        $notify_last_bidder->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }
}

header ('Location: ../product.php');
