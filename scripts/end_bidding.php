<?php

session_start();

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$_SESSION['closeBiddingSuccess'] = false;

// Get core data of the requested product
$prod_data_query = $connection->prepare('SELECT seller, highest_bidder
                                            FROM products
                                            WHERE id = :reqId');
$prod_data_query->bindParam(':reqId', $_SESSION['requestedProduct'], PDO::PARAM_INT);

try {
    $prod_data_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$prod_data = $prod_data_query->fetch(PDO::FETCH_ASSOC);

// If requesting user is the seller (so no illegal access to this script), close the bidding
if ($prod_data['seller'] == $_SESSION['userID']) {
    $new_status = true;
    $update_bidding_status_query = $connection->prepare('UPDATE products
                                            SET bidding_closed = :closed
                                            WHERE id = :id');
    $update_bidding_status_query->bindParam(':closed', $new_status, PDO::PARAM_BOOL);
    $update_bidding_status_query->bindParam(':id', $_SESSION['requestedProduct'], PDO::PARAM_INT);

    try {
        $update_bidding_status_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $_SESSION['closeBiddingSuccess'] = true;
}


if ($_SESSION['closeBiddingSuccess'])
{
    // If bidding is closed, notify the highest bidder that he won
    $date_time = time();

    $won_bidding_notif = 'won_bidding';

    $notify_last_bidder = $connection->prepare('INSERT INTO notifications(receiver_id, sender_id, type, about, date_time)
                                            VALUES (:receiver_id, :sender_id, :type, :about, :date_time)');
    $notify_last_bidder->bindParam(':receiver_id', $prod_data['highest_bidder'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':sender_id', $prod_data['seller'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':type', $won_bidding_notif, PDO::PARAM_STR, 16);
    $notify_last_bidder->bindParam(':about', $_SESSION['requestedProduct'], PDO::PARAM_INT);
    $notify_last_bidder->bindParam(':date_time', $date_time, PDO::PARAM_INT);

    try {
        $notify_last_bidder->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }


    // Notify the seller that he may rate the buyer
    $may_rate_notif = 'may_rate';

    $notify_may_rate = $connection->prepare('INSERT INTO notifications(receiver_id, sender_id, type, about, date_time)
                                            VALUES (:receiver_id, :sender_id, :type, :about, :date_time)');
    $notify_may_rate->bindParam(':receiver_id', $prod_data['seller'], PDO::PARAM_INT);
    $notify_may_rate->bindParam(':sender_id', $prod_data['highest_bidder'], PDO::PARAM_INT);
    $notify_may_rate->bindParam(':type', $may_rate_notif, PDO::PARAM_STR, 16);
    $notify_may_rate->bindParam(':about', $_SESSION['requestedProduct'], PDO::PARAM_INT);
    $notify_may_rate->bindParam(':date_time', $date_time, PDO::PARAM_INT);

    try {
        $notify_may_rate->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }


    // Also set a flag which allows the buyer to rate the seller
    $set_allow_rate_flag = $connection->prepare('INSERT INTO rating_flag(sender_id, receiver_id)
                                            VALUES (:sender_id, :receiver_id)');
    $set_allow_rate_flag->bindParam(':sender_id', $prod_data['seller'], PDO::PARAM_INT);
    $set_allow_rate_flag->bindParam(':receiver_id', $prod_data['highest_bidder'], PDO::PARAM_INT);

    try {
        $set_allow_rate_flag->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    // Vice-versa; seller may rate buyer
    $set_allow_rate_flag = $connection->prepare('INSERT INTO rating_flag(sender_id, receiver_id)
                                            VALUES (:sender_id, :receiver_id)');
    $set_allow_rate_flag->bindParam(':sender_id', $prod_data['highest_bidder'], PDO::PARAM_INT);
    $set_allow_rate_flag->bindParam(':receiver_id', $prod_data['seller'], PDO::PARAM_INT);

    try {
        $set_allow_rate_flag->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }
}

header ('Location: ../product.php');

?>