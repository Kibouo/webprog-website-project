<?php

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

// Count all notifications which were sent to the current player
$notifs_are_read = false;

$notifications_query = $connection->prepare('SELECT COUNT(receiver_id)
                                            FROM notifications
                                            WHERE receiver_id = :current_player AND read = :read');
$notifications_query->bindParam(':current_player', $_SESSION['userID'], PDO::PARAM_INT);
$notifications_query->bindParam(':read', $notifs_are_read, PDO::PARAM_BOOL);

try {
    $notifications_query->execute();
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}

$notifications = $notifications_query->fetch(PDO::FETCH_NUM);
$amt_notifications = $notifications[0];

if ($amt_notifications == 0)
{
    $amt_notifications = '';
}

?>