<?php

// Constructs a SQL statement based on the filtering choices made by the user
function buildFilterStatement($categories)
{
    $statement = 'SELECT *
                    FROM products ';

    // Search in X filter
    if (empty($_REQUEST['search_in']) || $_REQUEST['search_in'] == "title_descr") {
        $statement = $statement . 'WHERE (LOWER(title) LIKE LOWER(:reqString)
                    OR LOWER(descr) LIKE LOWER(:reqString)) ';
    } elseif ($_REQUEST['search_in'] == "title") {
        $statement = $statement . "WHERE (LOWER(title) LIKE LOWER(:reqString)) ";
    } elseif ($_REQUEST['search_in'] == "descr") {
        $statement = $statement . "WHERE (LOWER(descr) LIKE LOWER(:reqString)) ";
    }

    // Include items of category Y
    $amtBoxes = 0;
    for ($i = 0; $i < sizeof($categories); $i++) {
        if (isset($_REQUEST['cbox'.$i])) {
            $amtBoxes += 1;
        }
    }
    if ($amtBoxes > 0) {
        $statement = $statement . "AND ( ";
    }
    $addedBoxes = 0;
    for ($i = 0; $i < sizeof($categories); $i++) {
        if (isset($_REQUEST['cbox'.$i])) {
            if ($addedBoxes > 0) {
                $statement = $statement . "OR ";
            }
            $statement = $statement . "category = :cbox" . $i . " ";
            $addedBoxes += 1;
        }
    }
    if ($amtBoxes > 0) {
        $statement = $statement . ") ";
    }

    // Order by Z
    if (!empty($_REQUEST['order_by']))
    {
        if ($_REQUEST['order_by'] == "date_new") {
            $statement = $statement . "ORDER BY date_time DESC ";
        } elseif ($_REQUEST['order_by'] == "date_old") {
            $statement = $statement . "ORDER BY date_time ASC ";
        } elseif ($_REQUEST['order_by'] == "price_high") {
            $statement = $statement . "ORDER BY price DESC ";
        } elseif ($_REQUEST['order_by'] == "price_low") {
            $statement = $statement . "ORDER BY price ASC ";
        }
    }

    return $statement;
}

require_once 'globals.php';

try {
    $connection = new PDO('pgsql:host = ' . DB_HOST . '; dbname = '. DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $PDOException) {
    print "\nError: " . $PDOException->getMessage();
    die();
}


// Search for items specified by user filtering
if (strlen($_SESSION['search_string']) >= 1 && strlen($_SESSION['search_string']) <= 64) {
    $statement = buildFilterStatement($categories);

    // Get products with string in title & descr.
    $reqString = '%' . $_SESSION['search_string'] . '%';
    $prod_data_query = $connection->prepare($statement);
    $prod_data_query->bindParam(':reqString', $reqString, PDO::PARAM_STR, 64);
    for ($i = 0; $i < sizeof($categories); $i++) {
        if (isset($_REQUEST['cbox'.$i])) {
            $category_i = ':cbox' . $i;
            $prod_data_query->bindParam($category_i, $_REQUEST['cbox'.$i], PDO::PARAM_STR, 64);
        }
    }

    try {
        $prod_data_query->execute();
    } catch (PDOException $PDOException) {
        print "\nError: " . $PDOException->getMessage();
        die();
    }

    $prod_data = $prod_data_query->fetchAll(PDO::FETCH_ASSOC);
}

?>