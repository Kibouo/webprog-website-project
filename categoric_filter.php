<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
		<title>Ragtime.be | Categoric filter</title>
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/categoric_filter.css">
	</head>
	
	<body>
    <?php require_once 'scripts/page_default_data.php' ?>
    <?php require_once 'scripts/get_categories.php' ?>
    <?php if (isset($_SESSION['requestedCategory'])) {
            require_once 'scripts/get_prod_category.php';
}
    ?>

    <div id="spacer"></div>
    
    <div id="page_content">

    <?php

    if (isset($_SESSION['requestedCategory'])) {
        echo '
        <div id="category_filter">
            <h1>All products in ' . $_SESSION['requestedCategory'] . '</h1>
            <div id="filtered_products">';

        if (sizeof($prod_data) == 0) {
            echo '<p id="no_prod">No products found.</p>';
        } else {
            for ($i = 0; $i < sizeof($prod_data); $i++) {
                $tmp_id = $prod_data[$i]["id"];
                $tmp_picture = $prod_data[$i]["picture"];
                $tmp_price = number_format($prod_data[$i]["price"], 2, ",", " ");
                $tmp_category = $prod_data[$i]["category"];

                if (strlen($prod_data[$i]["title"]) > 16) {
                    $tmp_title = substr($prod_data[$i]["title"], 0, 16) . "...";
                } else {
                    $tmp_title = $prod_data[$i]["title"];
                }
                
                if (strlen($prod_data[$i]["descr"]) > 16) {
                    $tmp_description = substr($prod_data[$i]["descr"], 0, 16) . "...";
                } else {
                    $tmp_description = $prod_data[$i]["descr"];
                }

                echo '
                    <a href="scripts/set_requested_product.php?product_id=' . $tmp_id . '">
                        <div class="filtered_product">
                            <img alt="Product picture" class="picture" src=' . $tmp_picture . '>
                            <div class="prod_data">    
                                <h2 class="title">' . $tmp_title . '</h2>
                                <div class="price">Price: €' . $tmp_price . '</div>
                            </div>
                            <p class="description">Description: ' . $tmp_description . '</p>
                        </div>
                    </a>';
            }
        }
        echo '
            </div>
        </div>';
    } else {
        echo '
            <div id="choose_category">
                <h1>Choose a category</h1>
                <form id="category_dropdown" action="scripts/set_requested_category.php" method="post">
                    <select name="product_category">';

        for ($i = 0; $i < sizeof($categories); $i++) {
            $tmp = implode("", $categories[$i]);
            echo '
                        <option value=' . $tmp . '>' . $tmp . '</option>';
        }

        echo '
                    </select>
                    <input type="submit" id="set_req_category" name="set_req_category" value="Search!"/>
                </form>
            </div>';
    }
    ?>

    </div>
    
    <?php require_once 'header.php' ?>
    </body>
</html>
